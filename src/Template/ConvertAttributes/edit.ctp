<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConvertAttribute $convertAttribute
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $convertAttribute->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $convertAttribute->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Convert Attributes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="convertAttributes form large-9 medium-8 columns content">
    <?= $this->Form->create($convertAttribute) ?>
    <fieldset>
        <legend><?= __('Edit Convert Attribute') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('name_converted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
