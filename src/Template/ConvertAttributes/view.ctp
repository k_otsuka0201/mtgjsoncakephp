<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConvertAttribute $convertAttribute
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Convert Attribute'), ['action' => 'edit', $convertAttribute->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Convert Attribute'), ['action' => 'delete', $convertAttribute->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convertAttribute->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Convert Attributes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convert Attribute'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="convertAttributes view large-9 medium-8 columns content">
    <h3><?= h($convertAttribute->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($convertAttribute->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Converted') ?></th>
            <td><?= h($convertAttribute->name_converted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($convertAttribute->id) ?></td>
        </tr>
    </table>
</div>
