<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConvertAttribute[]|\Cake\Collection\CollectionInterface $convertAttributes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Convert Attribute'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="convertAttributes index large-9 medium-8 columns content">
    <h3><?= __('Convert Attributes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_converted') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($convertAttributes as $convertAttribute): ?>
            <tr>
                <td><?= $this->Number->format($convertAttribute->id) ?></td>
                <td><?= h($convertAttribute->name) ?></td>
                <td><?= h($convertAttribute->name_converted) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $convertAttribute->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $convertAttribute->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $convertAttribute->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convertAttribute->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
