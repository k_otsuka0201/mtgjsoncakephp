<nav class="navbar navbar-expand-sm navbar-default">

    <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="<?= $this->Url->build(['controller' => '/']) ?>"><img src="<?= $this->Url->build('/Sufee/images/logo.png') ?>" alt="Logo"></a>
        <a class="navbar-brand hidden" href="<?= $this->Url->build(['controller' => '/']) ?>"><img src="<?= $this->Url->build('/Sufee/images/logo2.png') ?>" alt="Logo"></a>
    </div>

    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="<?= $this->Url->build(['controller' => 'searches', 'action' => 'top']) ?>"> <i class="menu-icon fa fa-dashboard"></i>Searches</a>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->

    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <h3 class="menu-title">Data</h3>
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>各データ</a>
                <ul class="sub-menu children dropdown-menu">
                    <li><i class="fa fa-puzzle-piece"></i><a href="<?= $this->Url->build(['controller' => 'cards', 'action' => 'index']) ?>">カード</a></li>
                    <li><i class="fa fa-puzzle-piece"></i><a href="<?= $this->Url->build(['controller' => 'cardsets', 'action' => 'index']) ?>">カードセット</a></li>
                    <li><i class="fa fa-puzzle-piece"></i><a href="<?= $this->Url->build(['controller' => 'attributes', 'action' => 'index']) ?>">属性</a></li>
                    <li><i class="fa fa-puzzle-piece"></i><a href="<?= $this->Url->build(['controller' => 'convert-cardsets', 'action' => 'index']) ?>">カードセット変換</a></li>
                    <li><i class="fa fa-puzzle-piece"></i><a href="<?= $this->Url->build(['controller' => 'convert-attributes', 'action' => 'index']) ?>">属性変換</a></li>
                    <li><i class="fa fa-puzzle-piece"></i><a href="<?= $this->Url->build(['controller' => 'search-cards', 'action' => 'index']) ?>">カード検索データ</a></li>
                </ul>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->

    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <h3 class="menu-title">Links</h3>
            <li class="active">
                <a href="<?= $this->Url->build(['prefix' => false, 'controller' => '/', 'action' => '/']) ?>/sufee-admin-dashboard/index.html" target="_blank"> <i class="menu-icon fa fa-dashboard"></i>Sufee Admin Demo</a>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
