<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cardset[]|\Cake\Collection\CollectionInterface $cardsets
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Cardset'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cards Bigweb'), ['controller' => 'CardsBigweb', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cards Bigweb'), ['controller' => 'CardsBigweb', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cardsets index large-9 medium-8 columns content">
    <h3><?= __('Cardsets') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_jp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_kana') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('block') ?></th>
                <th scope="col"><?= $this->Paginator->sort('code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('format') ?></th>
                <th scope="col"><?= $this->Paginator->sort('baseSetSize') ?></th>
                <th scope="col"><?= $this->Paginator->sort('totalSetSize') ?></th>
                <th scope="col"><?= $this->Paginator->sort('isFoilOnly') ?></th>
                <th scope="col"><?= $this->Paginator->sort('release_date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cardsets as $cardset): ?>
            <tr>
                <td><?= $this->Number->format($cardset->id) ?></td>
                <td><?= h($cardset->name) ?></td>
                <td><?= h($cardset->name_jp) ?></td>
                <td><?= h($cardset->name_kana) ?></td>
                <td><?= h($cardset->type) ?></td>
                <td><?= h($cardset->block) ?></td>
                <td><?= h($cardset->code) ?></td>
                <td><?= h($cardset->format) ?></td>
                <td><?= $this->Number->format($cardset->baseSetSize) ?></td>
                <td><?= $this->Number->format($cardset->totalSetSize) ?></td>
                <td><?= h($cardset->isFoilOnly) ?></td>
                <td><?= h($cardset->release_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $cardset->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cardset->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cardset->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cardset->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
