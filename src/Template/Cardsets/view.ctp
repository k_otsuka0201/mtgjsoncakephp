<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cardset $cardset
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cardset'), ['action' => 'edit', $cardset->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cardset'), ['action' => 'delete', $cardset->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cardset->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cardsets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cardset'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cards Bigweb'), ['controller' => 'CardsBigweb', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cards Bigweb'), ['controller' => 'CardsBigweb', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cardsets view large-9 medium-8 columns content">
    <h3><?= h($cardset->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($cardset->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Jp') ?></th>
            <td><?= h($cardset->name_jp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Kana') ?></th>
            <td><?= h($cardset->name_kana) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($cardset->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Block') ?></th>
            <td><?= h($cardset->block) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($cardset->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Format') ?></th>
            <td><?= h($cardset->format) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cardset->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('BaseSetSize') ?></th>
            <td><?= $this->Number->format($cardset->baseSetSize) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TotalSetSize') ?></th>
            <td><?= $this->Number->format($cardset->totalSetSize) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Release Date') ?></th>
            <td><?= h($cardset->release_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IsFoilOnly') ?></th>
            <td><?= $cardset->isFoilOnly ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Cards Bigweb') ?></h4>
        <?php if (!empty($cardset->cards_bigweb)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artist') ?></th>
                <th scope="col"><?= __('Cmc') ?></th>
                <th scope="col"><?= __('Cardset Id') ?></th>
                <th scope="col"><?= __('Color Identity') ?></th>
                <th scope="col"><?= __('Colors') ?></th>
                <th scope="col"><?= __('Image Name') ?></th>
                <th scope="col"><?= __('Layout') ?></th>
                <th scope="col"><?= __('Legalities') ?></th>
                <th scope="col"><?= __('Mana Cost') ?></th>
                <th scope="col"><?= __('Mci Number') ?></th>
                <th scope="col"><?= __('Multiverseid') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Name Jp') ?></th>
                <th scope="col"><?= __('Name Kana') ?></th>
                <th scope="col"><?= __('Original Text') ?></th>
                <th scope="col"><?= __('Original Type') ?></th>
                <th scope="col"><?= __('Power') ?></th>
                <th scope="col"><?= __('Printings') ?></th>
                <th scope="col"><?= __('Rarity Id') ?></th>
                <th scope="col"><?= __('Rarity') ?></th>
                <th scope="col"><?= __('Rarity Jp') ?></th>
                <th scope="col"><?= __('Loyalty') ?></th>
                <th scope="col"><?= __('Number') ?></th>
                <th scope="col"><?= __('Rulings') ?></th>
                <th scope="col"><?= __('Subtypes') ?></th>
                <th scope="col"><?= __('Text') ?></th>
                <th scope="col"><?= __('Text Jp') ?></th>
                <th scope="col"><?= __('Toughness') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Type Jp') ?></th>
                <th scope="col"><?= __('Types') ?></th>
                <th scope="col"><?= __('Exp Code') ?></th>
                <th scope="col"><?= __('Cardtype Id') ?></th>
                <th scope="col"><?= __('Multiverseid Jp') ?></th>
                <th scope="col"><?= __('Cards New Id') ?></th>
                <th scope="col"><?= __('Series Id') ?></th>
                <th scope="col"><?= __('Condition Id') ?></th>
                <th scope="col"><?= __('Rear Face') ?></th>
                <th scope="col"><?= __('Related Name') ?></th>
                <th scope="col"><?= __('Related Card Id') ?></th>
                <th scope="col"><?= __('Image Stockid') ?></th>
                <th scope="col"><?= __('Colour Id') ?></th>
                <th scope="col"><?= __('Search Word') ?></th>
                <th scope="col"><?= __('Is Hide') ?></th>
                <th scope="col"><?= __('Is Foil Only') ?></th>
                <th scope="col"><?= __('HasFoil') ?></th>
                <th scope="col"><?= __('HasNonFoil') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($cardset->cards_bigweb as $cardsBigweb): ?>
            <tr>
                <td><?= h($cardsBigweb->id) ?></td>
                <td><?= h($cardsBigweb->artist) ?></td>
                <td><?= h($cardsBigweb->cmc) ?></td>
                <td><?= h($cardsBigweb->cardset_id) ?></td>
                <td><?= h($cardsBigweb->color_identity) ?></td>
                <td><?= h($cardsBigweb->colors) ?></td>
                <td><?= h($cardsBigweb->image_name) ?></td>
                <td><?= h($cardsBigweb->layout) ?></td>
                <td><?= h($cardsBigweb->legalities) ?></td>
                <td><?= h($cardsBigweb->mana_cost) ?></td>
                <td><?= h($cardsBigweb->mci_number) ?></td>
                <td><?= h($cardsBigweb->multiverseid) ?></td>
                <td><?= h($cardsBigweb->name) ?></td>
                <td><?= h($cardsBigweb->name_jp) ?></td>
                <td><?= h($cardsBigweb->name_kana) ?></td>
                <td><?= h($cardsBigweb->original_text) ?></td>
                <td><?= h($cardsBigweb->original_type) ?></td>
                <td><?= h($cardsBigweb->power) ?></td>
                <td><?= h($cardsBigweb->printings) ?></td>
                <td><?= h($cardsBigweb->rarity_id) ?></td>
                <td><?= h($cardsBigweb->rarity) ?></td>
                <td><?= h($cardsBigweb->rarity_jp) ?></td>
                <td><?= h($cardsBigweb->loyalty) ?></td>
                <td><?= h($cardsBigweb->number) ?></td>
                <td><?= h($cardsBigweb->rulings) ?></td>
                <td><?= h($cardsBigweb->subtypes) ?></td>
                <td><?= h($cardsBigweb->text) ?></td>
                <td><?= h($cardsBigweb->text_jp) ?></td>
                <td><?= h($cardsBigweb->toughness) ?></td>
                <td><?= h($cardsBigweb->type) ?></td>
                <td><?= h($cardsBigweb->type_jp) ?></td>
                <td><?= h($cardsBigweb->types) ?></td>
                <td><?= h($cardsBigweb->exp_code) ?></td>
                <td><?= h($cardsBigweb->cardtype_id) ?></td>
                <td><?= h($cardsBigweb->multiverseid_jp) ?></td>
                <td><?= h($cardsBigweb->cards_new_id) ?></td>
                <td><?= h($cardsBigweb->series_id) ?></td>
                <td><?= h($cardsBigweb->condition_id) ?></td>
                <td><?= h($cardsBigweb->rear_face) ?></td>
                <td><?= h($cardsBigweb->related_name) ?></td>
                <td><?= h($cardsBigweb->related_card_id) ?></td>
                <td><?= h($cardsBigweb->image_stockid) ?></td>
                <td><?= h($cardsBigweb->colour_id) ?></td>
                <td><?= h($cardsBigweb->search_word) ?></td>
                <td><?= h($cardsBigweb->is_hide) ?></td>
                <td><?= h($cardsBigweb->is_foil_only) ?></td>
                <td><?= h($cardsBigweb->hasFoil) ?></td>
                <td><?= h($cardsBigweb->hasNonFoil) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CardsBigweb', 'action' => 'view', $cardsBigweb->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CardsBigweb', 'action' => 'edit', $cardsBigweb->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CardsBigweb', 'action' => 'delete', $cardsBigweb->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cardsBigweb->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
