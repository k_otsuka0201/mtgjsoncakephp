<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cardset $cardset
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cardsets'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cards Bigweb'), ['controller' => 'CardsBigweb', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cards Bigweb'), ['controller' => 'CardsBigweb', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cardsets form large-9 medium-8 columns content">
    <?= $this->Form->create($cardset) ?>
    <fieldset>
        <legend><?= __('Add Cardset') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('name_jp');
            echo $this->Form->control('name_kana');
            echo $this->Form->control('type');
            echo $this->Form->control('block');
            echo $this->Form->control('code');
            echo $this->Form->control('format');
            echo $this->Form->control('baseSetSize');
            echo $this->Form->control('totalSetSize');
            echo $this->Form->control('isFoilOnly');
            echo $this->Form->control('release_date', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
