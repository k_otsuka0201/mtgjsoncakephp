<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Card $card
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cards'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Search Cards'), ['controller' => 'SearchCards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Search Card'), ['controller' => 'SearchCards', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cards form large-9 medium-8 columns content">
    <?= $this->Form->create($card) ?>
    <fieldset>
        <legend><?= __('Add Card') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('name_jp');
            echo $this->Form->control('name_kana');
            echo $this->Form->control('rarity');
            echo $this->Form->control('color');
            echo $this->Form->control('colorIdentity');
            echo $this->Form->control('colors');
            echo $this->Form->control('flavorText');
            echo $this->Form->control('flavorText_jp');
            echo $this->Form->control('text');
            echo $this->Form->control('text_jp');
            echo $this->Form->control('type');
            echo $this->Form->control('type_jp');
            echo $this->Form->control('types');
            echo $this->Form->control('layout');
            echo $this->Form->control('subtypes');
            echo $this->Form->control('supertypes');
            echo $this->Form->control('code');
            echo $this->Form->control('borderColor');
            echo $this->Form->control('frameVersion');
            echo $this->Form->control('keywords');
            echo $this->Form->control('faceName');
            echo $this->Form->control('side');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
