<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Card $card
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Card'), ['action' => 'edit', $card->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Card'), ['action' => 'delete', $card->id], ['confirm' => __('Are you sure you want to delete # {0}?', $card->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cards'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Card'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Search Cards'), ['controller' => 'SearchCards', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Search Card'), ['controller' => 'SearchCards', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cards view large-9 medium-8 columns content">
    <h3><?= h($card->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($card->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Jp') ?></th>
            <td><?= h($card->name_jp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Kana') ?></th>
            <td><?= h($card->name_kana) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rarity') ?></th>
            <td><?= h($card->rarity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Color') ?></th>
            <td><?= h($card->color) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ColorIdentity') ?></th>
            <td><?= h($card->colorIdentity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Colors') ?></th>
            <td><?= h($card->colors) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FlavorText') ?></th>
            <td><?= h($card->flavorText) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FlavorText Jp') ?></th>
            <td><?= h($card->flavorText_jp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($card->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type Jp') ?></th>
            <td><?= h($card->type_jp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Types') ?></th>
            <td><?= h($card->types) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Layout') ?></th>
            <td><?= h($card->layout) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subtypes') ?></th>
            <td><?= h($card->subtypes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supertypes') ?></th>
            <td><?= h($card->supertypes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($card->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('BorderColor') ?></th>
            <td><?= h($card->borderColor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FrameVersion') ?></th>
            <td><?= h($card->frameVersion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Keywords') ?></th>
            <td><?= h($card->keywords) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FaceName') ?></th>
            <td><?= h($card->faceName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Side') ?></th>
            <td><?= h($card->side) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($card->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Text') ?></h4>
        <?= $this->Text->autoParagraph(h($card->text)); ?>
    </div>
    <div class="row">
        <h4><?= __('Text Jp') ?></h4>
        <?= $this->Text->autoParagraph(h($card->text_jp)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Search Cards') ?></h4>
        <?php if (!empty($card->search_cards)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Card Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Offset') ?></th>
                <th scope="col"><?= __('Count') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($card->search_cards as $searchCards): ?>
            <tr>
                <td><?= h($searchCards->id) ?></td>
                <td><?= h($searchCards->name) ?></td>
                <td><?= h($searchCards->card_id) ?></td>
                <td><?= h($searchCards->type) ?></td>
                <td><?= h($searchCards->offset) ?></td>
                <td><?= h($searchCards->count) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SearchCards', 'action' => 'view', $searchCards->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SearchCards', 'action' => 'edit', $searchCards->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SearchCards', 'action' => 'delete', $searchCards->id], ['confirm' => __('Are you sure you want to delete # {0}?', $searchCards->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
