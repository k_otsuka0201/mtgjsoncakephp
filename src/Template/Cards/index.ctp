<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Card[]|\Cake\Collection\CollectionInterface $cards
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Card'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Search Cards'), ['controller' => 'SearchCards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Search Card'), ['controller' => 'SearchCards', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cards index large-9 medium-8 columns content">
    <h3><?= __('Cards') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_jp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_kana') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rarity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('color') ?></th>
                <th scope="col"><?= $this->Paginator->sort('colorIdentity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('colors') ?></th>
                <th scope="col"><?= $this->Paginator->sort('flavorText') ?></th>
                <th scope="col"><?= $this->Paginator->sort('flavorText_jp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type_jp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('types') ?></th>
                <th scope="col"><?= $this->Paginator->sort('layout') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subtypes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supertypes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('borderColor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('frameVersion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('keywords') ?></th>
                <th scope="col"><?= $this->Paginator->sort('faceName') ?></th>
                <th scope="col"><?= $this->Paginator->sort('side') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cards as $card): ?>
            <tr>
                <td><?= $this->Number->format($card->id) ?></td>
                <td><?= h($card->name) ?></td>
                <td><?= h($card->name_jp) ?></td>
                <td><?= h($card->name_kana) ?></td>
                <td><?= h($card->rarity) ?></td>
                <td><?= h($card->color) ?></td>
                <td><?= h($card->colorIdentity) ?></td>
                <td><?= h($card->colors) ?></td>
                <td><?= h($card->flavorText) ?></td>
                <td><?= h($card->flavorText_jp) ?></td>
                <td><?= h($card->type) ?></td>
                <td><?= h($card->type_jp) ?></td>
                <td><?= h($card->types) ?></td>
                <td><?= h($card->layout) ?></td>
                <td><?= h($card->subtypes) ?></td>
                <td><?= h($card->supertypes) ?></td>
                <td><?= h($card->code) ?></td>
                <td><?= h($card->borderColor) ?></td>
                <td><?= h($card->frameVersion) ?></td>
                <td><?= h($card->keywords) ?></td>
                <td><?= h($card->faceName) ?></td>
                <td><?= h($card->side) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $card->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $card->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $card->id], ['confirm' => __('Are you sure you want to delete # {0}?', $card->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
