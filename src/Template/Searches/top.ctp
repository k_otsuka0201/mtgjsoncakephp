<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="animated fadeIn">
    <div class="row">

        <div class="col-md-12">

            <?= $this->Flash->render('sufee') ?>

            <div class="row">
                <div class="col-sm-6">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Work Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="bootstrap-data-table-export_filter" class="dataTables_filter">
                                        <h4 class="mb-3">Search</h4>
                                        <input type="search" name="keywords" id="mtg-search-input" class="form-control form-control-sm mb-3" placeholder="" aria-controls="bootstrap-data-table-export" value="<?= $searching['request_data']['keywords'] ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div id="results" class="col-sm-12">
                                    <?php if (!$results) { ?>
                                        該当するデータはありません
                                    <?php } else { ?>
                                        <?php foreach ($results as $result_key => $cards) { ?>
                                            <h4 class="mb-3"><?= $result_key ?> (<?= $cards['count'] ?>)</h4>
                                            <?php if (isset($cards['record_ids'])) { ?>
                                                <?php foreach ($cards['record_ids'] as $card_id) { ?>
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="stat-widget-six">
                                                                <div class="stat-icon dib">
                                                                    <i class="ti-layers text-muted" style="font-size: 24px;"></i>
                                                                </div>
                                                                <div class="stat-content text-left">
                                                                    <div class="dib">
                                                                        <div class="stat-heading">
                                                                            <?= $card_data[$card_id]['name'] ?> <?= $card_data[$card_id]['name_jp'] ?><br>
                                                                            <?= $card_data[$card_id]['code'] ?> （<?= $card_id ?>）<br>
                                                                            <small class="text-muted"><?= $card_data[$card_id]['name_kana'] ?></small>
                                                                        </div>
                                                                        <hr>
                                                                        <div class="stat-text text-muted">
                                                                            Rarity: <?= ucfirst($card_data[$card_id]['rarity']) ?> /
                                                                            Color: <?= ucfirst($card_data[$card_id]['color']) ?> /
                                                                            Colors: <?= ucfirst($card_data[$card_id]['colors']) ?> /
                                                                            ColorIdentity: <?= ucfirst($card_data[$card_id]['colorIdentity']) ?> /
                                                                            type: <?= ucfirst($card_data[$card_id]['type']) ?> /
                                                                            type_jp: <?= ucfirst($card_data[$card_id]['type_jp']) ?> /
                                                                            types: <?= ucfirst($card_data[$card_id]['types']) ?> /
                                                                            subtypes: <?= ucfirst($card_data[$card_id]['subtypes']) ?> /
                                                                            supertypes: <?= ucfirst($card_data[$card_id]['supertypes']) ?> /
                                                                            layout: <?= ucfirst($card_data[$card_id]['layout']) ?> /
                                                                            borderColor: <?= ucfirst($card_data[$card_id]['borderColor']) ?> /
                                                                            frameVersion: <?= ucfirst($card_data[$card_id]['frameVersion']) ?> /
                                                                            faceName: <?= ucfirst($card_data[$card_id]['faceName']) ?> /
                                                                            side: <?= ucfirst($card_data[$card_id]['side']) ?> /
                                                                        </div>
                                                                        <div class="stat-text">Keywords: <?= $card_data[$card_id]['keywords'] ?></div>
                                                                        <div class="stat-text text-muted"><?= $card_data[$card_id]['flavorText'] ?></div>
                                                                        <div class="stat-text text-muted"><?= $card_data[$card_id]['flavorText_jp'] ?></div>
                                                                        <div class="stat-text text-muted"><?= $card_data[$card_id]['text'] ?></div>
                                                                        <div class="stat-text text-muted"><?= $card_data[$card_id]['text_jp'] ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Request Data</strong>
                        </div>
                        <div class="card-body">
                            <span id="request-data"><?= $searching['request_data']['keywords'] ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Search Data</strong>
                        </div>
                        <div class="card-body">
                            <div id="search-data">
                            <?php foreach ($searching['search_data']['keywords'] as $keyword) { ?>
                                <div><?= $keyword ?></div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->Form->create(null, [
            'id' => 'mtg-search',
        ]) ?>
        <?= $this->Form->control('keywords', [
            'type' => 'hidden',
            'id' => 'mtg-search-keywords',
        ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<script src="<?= $this->Url->build('/Sufee/vendors/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/popper.js/dist/umd/popper.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/assets/js/main.js') ?>"></script>

<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/jszip/dist/jszip.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/pdfmake/build/vfs_fonts.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/buttons.colVis.min.js') ?>"></script>
<!--<script src="--><?php // $this->Url->build('/Sufee/assets/js/init-scripts/data-table/datatables-init.js') ?><!--"></script>-->

<script type="text/javascript">
    jQuery(function() {
        let on_ime = false;
        let off_ime = false;

        jQuery('#mtg-search-input').on('keyup', _debounce(function (event) {
            if (on_ime) {
                return false;
            } else {
                if (off_ime) {
                    off_ime = false;
                    return false;
                }
            }
            console.log('keyup', event.keyCode);
            if (event.keyCode == 13) {
                jQuery('#mtg-search-keywords').val(jQuery(this).val());
                jQuery('#mtg-search').submit();
            } else {
                ajaxSending();
            }
        }, 500));

        jQuery('#mtg-search-input').on('compositionstart', function (event) {
            console.log('compositionstart');
            on_ime = true;
        });

        jQuery('#mtg-search-input').on('compositionend', function (event) {
            console.log('compositionend');
            on_ime = false;
            off_ime = true;
        });
    });

    function ajaxSending () {
        const keywords = jQuery('#mtg-search-input').val();

        jQuery.ajax({
            url	: '/caketest/searches/ajax_top',
            type	: 'POST',
            data	: {keywords: keywords},
            dataType: 'json',
            timeout : 10000,
            error	: function(xhr,status,errorThrown){
                alert(status);
                console.log(xhr);
                console.log(status);
                console.log(errorThrown);
            },
            success	: function(data,status,xhr){
                console.log(data);
                execute(data);
            }
        });
    }

    function execute (data) {
        setRequestData(data['searching']['request_data']);
        setSearchData(data['searching']['search_data']);
        setResults(data['results']);
    }

    function setRequestData (data) {
        jQuery('#request-data').html(data.keywords);
    }

    function setSearchData (data) {
        jQuery('#search-data').empty();
        jQuery.each(data.keywords, function (index, value) {
            jQuery('#search-data').append('<div>' + value + '</div>');
        });
    }

    function setResults (data) {
        jQuery('#results').empty();
        if (!Object.keys(data).length) {
            jQuery('#results').html('該当するデータはありません');
        } else {
            jQuery.each(data, function (property, value) {
                jQuery('#results').append('<h4 class="mb-3">' + property + ' (' + value.count + ')</h4>');
                if (value.records) {
                    jQuery.each(value.records, function (index, value) {
                        const html = renderCardData(value);
                        jQuery('#results').append(html);
                    });
                }
            });
        }
    }

    function renderCardData(data) {
        let html = '';
        html+= '<div class="card">';
            html+= '<div class="card-body">';
                html+= '<div class="stat-widget-six">';
                    html+= '<div class="stat-icon dib">';
                        html+= '<i class="ti-layers text-muted" style="font-size: 24px;"></i>';
                    html+= '</div>';
                    html+= '<div class="stat-content text-left">';
                        html+= '<div class="dib">';
                            html+= '<div class="stat-heading">';
                            html+= data.name + ' ' + data.name_jp + '<br>';
                            html+= data.code + ' ' + data.id + '<br>';
                            html+= '<small class="text-muted">' + data.name_kana + '</small>';
                            html+= '';
                            html+= '';
                            html+= '</div>';
                        html+= '</div>';
                    html+= '</div>';
                html+= '</div>';
            html+= '</div>';
        html+= '</div>';
        html+= '';

        return html;
    }

    var _debounce = function(func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        var later = function() {
            var last = _now() - timestamp;
            if (last < wait && last >= 0) {
                timeout = setTimeout(later, wait - last);
            } else {
                timeout = null;
                if (!immediate) {
                    result = func.apply(context, args);
                    if (!timeout) context = args = null;
                }
            }
        };
        var _now = Date.now || function(){
            return new Date().getTime();
        };

        return function() {
            context = this;
            args = arguments;
            timestamp = _now();
            var callNow = immediate && !timeout;
            if (!timeout) timeout = setTimeout(later, wait);
            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }
            return result;
        };
    };

</script>
