<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="animated fadeIn">
    <div class="row">

        <div class="col-md-12">

            <?= $this->Flash->render('sufee') ?>

            <div class="row">
                <div class="col-sm-6">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Work Table</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="bootstrap-data-table-export_filter" class="dataTables_filter">
                                        <h4 class="mb-3">Search</h4>
                                        <input type="search" name="keywords" id="mtg-search-input" class="form-control form-control-sm mb-3" placeholder="" aria-controls="bootstrap-data-table-export" value="<?= $searching['request_data']['keywords'] ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="bootstrap-data-table-export_info" role="status" aria-live="polite">
                                        <div class="mt-2">
                                            Showing <span id="record-from"><?= $record_from ?></span>
                                            to <span id="record-to"><?= $record_to ?></span>
                                             of <span id="result-total-count"><?= $result_total_count ?></span>
                                             records
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table-export_paginate">
                                        <ul class="pagination justify-content-end">
                                            <li class="paginate_button page-item previous <?= (!is_null($page_prev)) ? '' : 'disabled' ?>" id="bootstrap-data-table-export_previous">
                                                <a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="<?= $page_prev ?>" tabindex="0" class="page-link">≪</a>
                                            </li>
                                            <li class="paginate_button page-item next <?= (!is_null($page_next)) ? '' : 'disabled' ?>" id="bootstrap-data-table-export_next">
                                                <a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="<?= $page_next ?>" tabindex="0" class="page-link">≫</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="results" class="col-sm-12">
                                    <?php if (!$card_data) { ?>
                                        該当するデータはありません
                                    <?php } else { ?>
                                        <?php
                                        foreach ($card_data as $card_datum) {
                                            $card_id = $card_datum['id'];
                                        ?>
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="stat-widget-six">
                                                        <div class="stat-icon dib">
                                                            <i class="ti-layers text-muted" style="font-size: 24px;"></i>
                                                        </div>
                                                        <div class="stat-content text-left">
                                                            <div class="dib">
                                                                <div class="stat-heading">
                                                                    <?= $card_datum['name'] ?> <?= $card_datum['name_jp'] ?><br>
                                                                    <?= $card_datum['code'] ?> （<?= $card_id ?>）<br>
                                                                    <small class="text-muted"><?= $card_datum['name_kana'] ?></small>
                                                                </div>
                                                                <hr>
                                                                <div class="stat-text text-muted">
                                                                    Rarity: <?= ucfirst($card_datum['rarity']) ?> /
                                                                    Color: <?= ucfirst($card_datum['color']) ?> /
                                                                    Colors: <?= ucfirst($card_datum['colors']) ?> /
                                                                    ColorIdentity: <?= ucfirst($card_datum['colorIdentity']) ?> /
                                                                    type: <?= ucfirst($card_datum['type']) ?> /
                                                                    type_jp: <?= ucfirst($card_datum['type_jp']) ?> /
                                                                    types: <?= ucfirst($card_datum['types']) ?> /
                                                                    subtypes: <?= ucfirst($card_datum['subtypes']) ?> /
                                                                    supertypes: <?= ucfirst($card_datum['supertypes']) ?> /
                                                                    layout: <?= ucfirst($card_datum['layout']) ?> /
                                                                    borderColor: <?= ucfirst($card_datum['borderColor']) ?> /
                                                                    frameVersion: <?= ucfirst($card_datum['frameVersion']) ?> /
                                                                    faceName: <?= ucfirst($card_datum['faceName']) ?> /
                                                                    side: <?= ucfirst($card_datum['side']) ?> /
                                                                </div>
                                                                <div class="stat-text">Keywords: <?= $card_datum['keywords'] ?></div>
                                                                <div class="stat-text text-muted"><?= $card_datum['flavorText'] ?></div>
                                                                <div class="stat-text text-muted"><?= $card_datum['flavorText_jp'] ?></div>
                                                                <div class="stat-text text-muted"><?= $card_datum['text'] ?></div>
                                                                <div class="stat-text text-muted"><?= $card_datum['text_jp'] ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Request Data</strong>
                        </div>
                        <div class="card-body">
                            <span id="request-data"><?= $searching['request_data']['keywords'] ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Search Data</strong>
                        </div>
                        <div class="card-body">
                            <div id="search-data">
                            <?php if ($searching['search_data']['keywords']) { ?>
                                <?php foreach ($searching['search_data']['keywords'] as $keyword) { ?>
                                    <div><?= $keyword ?></div>
                                <?php } ?>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->Form->create(null, [
            'type' => 'get',
            'id' => 'mtg-search',
        ]) ?>
        <?= $this->Form->control('searchmode', [
            'type' => 'hidden',
            'id' => 'mtg-search-searchmode',
            'value' => $searching['request_data']['searchmode'],
        ]) ?>
        <?= $this->Form->control('keywords', [
            'type' => 'hidden',
            'id' => 'mtg-search-keywords',
        ]) ?>
        <?= $this->Form->control('page', [
            'type' => 'hidden',
            'id' => 'mtg-search-page',
            'value' => $page,
        ]) ?>
        <?= $this->Form->control('sequence', [
            'type' => 'hidden',
            'id' => 'mtg-search-sequence',
        ]) ?>
        <?= $this->Form->control('category', [
            'type' => 'hidden',
            'id' => 'mtg-search-category',
        ]) ?>
        <?= $this->Form->control('kind', [
            'type' => 'hidden',
            'id' => 'mtg-search-kind',
        ]) ?>
        <?= $this->Form->control('convert_id', [
            'type' => 'hidden',
            'id' => 'mtg-search-convert-id',
        ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<script src="<?= $this->Url->build('/Sufee/vendors/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/popper.js/dist/umd/popper.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/assets/js/main.js') ?>"></script>

<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/jszip/dist/jszip.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/pdfmake/build/vfs_fonts.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
<script src="<?= $this->Url->build('/Sufee/vendors/datatables.net-buttons/js/buttons.colVis.min.js') ?>"></script>
<!--<script src="--><?php // $this->Url->build('/Sufee/assets/js/init-scripts/data-table/datatables-init.js') ?><!--"></script>-->

<script type="text/javascript">
    jQuery(function() {
        let on_ime = false;
        let off_ime = false;

        jQuery('#mtg-search-input').on('keyup', _debounce(function (event) {
            if (on_ime) {
                return false;
            }
            console.log('keyup', event.keyCode);
            if (event.keyCode == 13) {
                jQuery('#mtg-search-searchmode').val('fulltext');
                jQuery('#mtg-search-keywords').val(jQuery(this).val());
                jQuery('#mtg-search-page').val(1);
                jQuery('#mtg-search').submit();
            } else {
                jQuery('#mtg-search-keywords').val(jQuery(this).val());
                jQuery('#mtg-search-page').val(1);
                ajaxSending();
            }
        }, 500));

        jQuery('#mtg-search-input').on('compositionstart', function (event) {
            on_ime = true;
        });

        jQuery('#mtg-search-input').on('compositionend', function (event) {
            on_ime = false;
        });

        jQuery('#results').on('click', '.card.candidate', function (event){
            const sequence = jQuery(this).data('sequence');
            const category = jQuery(this).data('category');
            const kind = jQuery(this).data('kind');
            const covert_id = jQuery(this).data('convert-id');

            jQuery('#mtg-search-searchmode').val('candidate');
            // Submitでキーワードがクリアされるので、sequenceをセットする
            jQuery('#mtg-search-keywords').val(sequence);
            jQuery('#mtg-search-page').val(1);
            jQuery('#mtg-search-sequence').val(sequence);
            jQuery('#mtg-search-category').val(category);
            jQuery('#mtg-search-kind').val(kind);
            jQuery('#mtg-search-convert-id').val(covert_id);
            jQuery('#mtg-search').submit();
        });

        jQuery('.pagination').on('click', 'li.paginate_button:not(".disabled,.active")', function () {
            const request_page = jQuery(this).find('a').data('dt-idx');
            console.log(request_page);
            jQuery('#mtg-search-keywords').val(jQuery('#mtg-search-input').val());
            jQuery('#mtg-search-page').val(request_page);
            jQuery('#mtg-search').submit();
        });
    });

    jQuery(window).on('load', function () {
        const mtg_search_input = jQuery('#mtg-search-input');
        const keywords_is_set = mtg_search_input.val();
        jQuery('#mtg-search-input').val('').focus().val(keywords_is_set);
    });

    let kinds_array = [];

    function ajaxSending () {
        const keywords = jQuery('#mtg-search-keywords').val();
        const page = jQuery('#mtg-search-page').val();

        jQuery.ajax({
            url	: '/caketest/api/full-text/search',
            type	: 'POST',
            data	: {keywords: keywords, page: page},
            dataType: 'json',
            timeout : 10000,
            error	: function(xhr,status,errorThrown){
                alert(status);
                console.log(xhr);
                console.log(status);
                console.log(errorThrown);
            },
            success	: function(data,status,xhr){
                console.log(data);
                execute(data);
            }
        });
    }

    function execute (data) {
        kinds_array = data['kinds'];

        setRequestData(data['searching']['request_data']);
        setSearchData(data['searching']['search_data']);
        setResultTotalCount(data['result_total_count']);
        setPagePrev(false);
        setPageNext(false);
        setRecordFrom(data['record_from']);
        setRecordTo(data['record_to']);
        setPointResults(data['point_data']);
        setAnalyzedResults(data['results'], data['analyzed_data']);
    }

    function setRequestData (data) {
        jQuery('#request-data').html(data.keywords);
    }

    function setSearchData (data) {
        jQuery('#search-data').empty();
        jQuery.each(data.keywords, function (index, value) {
            jQuery('#search-data').append('<div>' + value + '</div>');
        });
    }

    function setResultTotalCount (data) {
        jQuery('#result-total-count').text(data);
    }

    function setPagePrev (data) {
        if (data) {
            setPagePrevClass(true);
            setPagePrevLink(data);
        } else {
            setPagePrevClass(false);
            setPagePrevLink(0);
        }
    }

    function setPagePrevClass (is_able) {
        if (is_able) {
            jQuery('.page-item.previous').removeClass('disabled');
        } else {
            jQuery('.page-item.previous').addClass('disabled');
        }
    }

    function setPagePrevLink (data) {
        jQuery('.page-item.previous a').data('dt-idx', data).attr('data-dt-idx', data);
    }

    function setPageNext (data) {
        if (data) {
            setPageNextClass(true);
            setPageNextLink(data);
        } else {
            setPageNextClass(false);
            setPageNextLink();
        }
    }

    function setPageNextClass (is_able) {
        if (is_able) {
            jQuery('.page-item.next').removeClass('disabled');
        } else {
            jQuery('.page-item.next').addClass('disabled');
        }
    }

    function setPageNextLink (data) {
        jQuery('.page-item.next a').data('dt-idx', data).attr('data-dt-idx', data);
    }

    function setRecordFrom (data) {
        jQuery('#record-from').text(data);
    }

    function setRecordTo (data) {
        jQuery('#record-to').text(data);
    }

    function setAnalyzedResults (results, analyzed_data) {
        // jQuery('#results').empty();
        if (!Object.keys(analyzed_data).length) {
            jQuery('#results').html('該当するデータはありません');
        } else {
            jQuery.each(analyzed_data, function (property, value) {
                const html = renderAnalyzedData(value);
                console.log(value);
                jQuery('#results').append(html);
            });
        }
    }

    function renderAnalyzedData(data) {
        let html = '';
        jQuery.each(data.groups, function (property, value) {
            html+= '<div class="card candidate" data-sequence="' + data.sequence + '" data-category="' + value.category + '" data-kind="' + value.kind + '" data-count="' + value.cnt + '" data-convert-id="0">';
            html+= '<div class="card-body">';
            html+= '<div class="stat-widget-six">';
            html+= '<div class="stat-content text-left">';
            html+= '<div class="dib">';
            html+= '<div class="stat-heading">';
            html+= kinds_array[value.kind] + ' : ' + data.sequence + '(' + value.cnt + ')';
            html+= '</div>';
            html+= '</div>';
            html+= '</div>';
            html+= '</div>';
            html+= '</div>';
            html+= '</div>';
        });

        return html;
    }

    function setPointResults (results, card_data) {
        jQuery('#results').empty();
        if (!Object.keys(results).length) {
            jQuery('#results').html('該当するデータはありません');
        } else {
            jQuery.each(results, function (property, value) {
                // jQuery('#results').append('<h4 class="mb-3">' + value.kind + '</h4>');
                const html = renderCardData(value);
                jQuery('#results').append(html);
            });
        }
    }

    function renderCardData(data) {
        let html = '';
        html+= '<div class="card candidate" data-sequence="" data-category="0" data-kind="0" data-count="0" data-convert-id="' + data.id + '">';
        html+= '<div class="card-body">';
        html+= '<div class="stat-widget-six">';
        html+= '<div class="stat-icon dib">';
        html+= '<i class="ti-layers text-muted" style="font-size: 24px;"></i>';
        html+= '</div>';
        html+= '<div class="stat-content text-left">';
        html+= '<div class="dib">';
        html+= '<div class="stat-heading">';
        html+= data.name + ' ' + data.name_jp + '<br>';
        html+= data.code + ' ' + data.id + '<br>';
        html+= '<small class="text-muted">' + data.name_kana + '</small>';
        html+= '';
        html+= '';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '';

        return html;
    }

    var _debounce = function(func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        var later = function() {
            var last = _now() - timestamp;
            if (last < wait && last >= 0) {
                timeout = setTimeout(later, wait - last);
            } else {
                timeout = null;
                if (!immediate) {
                    result = func.apply(context, args);
                    if (!timeout) context = args = null;
                }
            }
        };
        var _now = Date.now || function(){
            return new Date().getTime();
        };

        return function() {
            context = this;
            args = arguments;
            timestamp = _now();
            var callNow = immediate && !timeout;
            if (!timeout) timeout = setTimeout(later, wait);
            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }
            return result;
        };
    };

</script>
