<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SearchCard $searchCard
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Search Card'), ['action' => 'edit', $searchCard->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Search Card'), ['action' => 'delete', $searchCard->id], ['confirm' => __('Are you sure you want to delete # {0}?', $searchCard->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Search Cards'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Search Card'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cards'), ['controller' => 'Cards', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Card'), ['controller' => 'Cards', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="searchCards view large-9 medium-8 columns content">
    <h3><?= h($searchCard->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($searchCard->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Card') ?></th>
            <td><?= $searchCard->has('card') ? $this->Html->link($searchCard->card->name, ['controller' => 'Cards', 'action' => 'view', $searchCard->card->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($searchCard->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($searchCard->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Offset') ?></th>
            <td><?= $this->Number->format($searchCard->offset) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Count') ?></th>
            <td><?= $this->Number->format($searchCard->count) ?></td>
        </tr>
    </table>
</div>
