<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SearchCard $searchCard
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Search Cards'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cards'), ['controller' => 'Cards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Card'), ['controller' => 'Cards', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="searchCards form large-9 medium-8 columns content">
    <?= $this->Form->create($searchCard) ?>
    <fieldset>
        <legend><?= __('Add Search Card') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('card_id', ['options' => $cards]);
            echo $this->Form->control('type');
            echo $this->Form->control('offset');
            echo $this->Form->control('count');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
