<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConvertCardset $convertCardset
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Convert Cardsets'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="convertCardsets form large-9 medium-8 columns content">
    <?= $this->Form->create($convertCardset) ?>
    <fieldset>
        <legend><?= __('Add Convert Cardset') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('name_converted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
