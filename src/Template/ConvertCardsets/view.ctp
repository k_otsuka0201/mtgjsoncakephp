<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConvertCardset $convertCardset
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Convert Cardset'), ['action' => 'edit', $convertCardset->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Convert Cardset'), ['action' => 'delete', $convertCardset->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convertCardset->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Convert Cardsets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convert Cardset'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="convertCardsets view large-9 medium-8 columns content">
    <h3><?= h($convertCardset->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($convertCardset->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Converted') ?></th>
            <td><?= h($convertCardset->name_converted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($convertCardset->id) ?></td>
        </tr>
    </table>
</div>
