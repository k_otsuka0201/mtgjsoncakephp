<?php

?>
<?= $this->Form->create() ?>
<?= $this->Form->control('keywords') ?>
<?= $this->Form->end() ?>
<?php
if ($result) {
    foreach ($result as $key => $data) {
        if ($key === 'attributes') {
            echo '各検索ワードに一致する属性データ<br>';
            foreach ($data as $datum) {
                echo $datum->id . ' / ';
                echo $datum->name . ' / ';
                echo $datum->type;
                echo '<hr>';
            }
            echo '<hr>';
        } elseif ($key === 'cardnames') {
            echo 'カード名に完全一致するカードデータ<br>';
            foreach ($data as $datum) {
                echo $datum->id . ' / ';
                echo $datum->name . ' / ';
                echo $datum->name_jp . ' / ';
                echo $datum->rarity . ' / ';
                echo $datum->color . ' / ';
                echo $datum->colorIdentity . ' / ';
                echo $datum->colors;
                echo '<hr>';
            }
            echo '<hr>';
        } elseif ($key === 'cards') {
            echo '各検索ワードを全て含むカードデータ<br>';
            foreach ($data as $datum) {
                echo $datum->id . ' / ';
                echo $datum->name . ' / ';
                echo $datum->name_jp . ' / ';
                echo $datum->rarity . ' / ';
                echo $datum->color . ' / ';
                echo $datum->colorIdentity . ' / ';
                echo $datum->colors;
                echo '<hr>';
            }
        } else {
            echo $key . 'が部分一致<br>';
            foreach ($data as $datum) {
                echo $datum->id . ' / ';
                echo $datum->name . ' / ';
                echo $datum->name_jp . ' / ';
                echo $datum->rarity . ' / ';
                echo $datum->color . ' / ';
                echo $datum->colorIdentity . ' / ';
                echo $datum->colors;
                echo '<hr>';
            }
        }
    }
}
?>
