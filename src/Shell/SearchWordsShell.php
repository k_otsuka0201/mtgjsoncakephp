<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * SearchWords shell command.
 */
class SearchWordsShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Attributes');
        $this->loadModel('Cards');
        $this->loadModel('Cardsets');
        $this->loadModel('SearchWords');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $connection = ConnectionManager::get('default');
        $connection->execute('TRUNCATE TABLE search_words');

        $cardsets = $this->Cardsets
            ->find('all')
            ->where(['name_kana IS NOT NULL'])
            ->toArray();
//        foreach (array_slice($cards, 0, 5) as $card) {
        foreach ($cardsets as $cardset) {
            debug($cardset->name);

            $this->createCode($cardset);
            $this->createName($cardset);
            $this->createNameJp($cardset);
        }
    }

    public function createCode($cardset)
    {

        $new_entity = $this->SearchWords->newEntity();
        $new_entity->name = $cardset->code;
        $new_entity->fullname = $cardset->name;
        $new_entity->cardset_id = $cardset->id;
        $new_entity->next = 0;
        $new_entity->is_terminal = 1;
        if (!$result = $this->SearchWords->save($new_entity)) {
            $this->abort('Error: createCode');
        }
    }

    public function createName($cardset)
    {
        $next = 0;
        $search_words = $this->SearchWords->find('all');
        $query = $search_words->select([
            'max_id' => $search_words->func()->max('id')
            ])
            ->first();
        $max_id = $query->max_id;

        $name_exploded = $this->nameExplode($cardset->name);

        $name_size = count($name_exploded);
        $count = 1;
        foreach ($name_exploded as $name) {
            $count++;

            $new_entity = $this->SearchWords->newEntity();
            $new_entity->name = $name;
            $new_entity->fullname = $cardset->name;
            $new_entity->cardset_id = $cardset->id;
            if ($count > $name_size) {
                $new_entity->next = 0;
                $new_entity->is_terminal = 1;
            } else {
                $new_entity->next = $max_id + $count;
                $new_entity->is_terminal = 0;
            }
            if (!$result = $this->SearchWords->save($new_entity)) {
                $this->abort('Error: createCode');
            }
        }
    }

    public function createNameJp($cardset)
    {
        // 日本語名は半角区切りで作成

        $next = 0;
        $search_words = $this->SearchWords->find('all');
        $query = $search_words->select([
            'max_id' => $search_words->func()->max('id')
            ])
            ->first();
        $max_id = $query->max_id;

        $namejp_exploded = $this->nameExplode($cardset->name_jp);

        $name_size = count($namejp_exploded);
        $count = 1;
        foreach ($namejp_exploded as $name) {
            $count++;

            $new_entity = $this->SearchWords->newEntity();
            $new_entity->name = $name;
            $new_entity->fullname = $cardset->name_jp;
            $new_entity->cardset_id = $cardset->id;
            if ($count > $name_size) {
                $new_entity->next = 0;
                $new_entity->is_terminal = 1;
            } else {
                $new_entity->next = $max_id + $count;
                $new_entity->is_terminal = 0;
            }
            if (!$result = $this->SearchWords->save($new_entity)) {
                $this->abort('Error: createCode');
            }
        }

    }

    public function nameExplode ($name)
    {

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $name_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($name, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $name_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $name_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        return preg_split('/[\s]+/', $name_removed, -1, PREG_SPLIT_NO_EMPTY);
    }

}
