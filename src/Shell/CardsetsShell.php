<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * Cardsets shell command.
 */
class CardsetsShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Attributes');
        $this->loadModel('Cards');
        $this->loadModel('CardsetsConvert');
        $this->loadModel('Cardsets');
        $this->loadModel('Searches');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $connection = ConnectionManager::get('default');
        $connection->execute('TRUNCATE TABLE cardsets_convert');

        $cardsets = $this->Cardsets->find('all')->toArray();
//        foreach (array_slice($cardsets, 0, 5) as $cardset) {
        foreach ($cardsets as $cardset) {
            $name = $cardset->name;
            $name_jp = ($cardset->name_jp) ? $cardset->name_jp : null ;
            $code = $cardset->code;
            $this->createConvertData($name, $code);
            if ($name_jp) {
                $this->createConvertData($name_jp, $code);
            }
            $this->createConvertData($code, $code);
        }

//        debug($cards);
    }

    public function createConvertData ($name, $code) {
        $new_entity = $this->CardsetsConvert->newEntity();
        $new_entity->name = $name;
        $new_entity->name_converted = $code;
        if ($result = $this->CardsetsConvert->save($new_entity)) {
            return $result->id;
        } else {
            $this->abort('Error: createConvertData');
        }
    }
}
