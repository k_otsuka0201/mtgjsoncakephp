<?php
namespace App\Shell;

use Cake\Console\Shell;

/**
 * UpdateKanaOfDuplex shell command.
 */
class UpdateKanaOfDuplexShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Cards');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $cards = $this->Cards->find('all')
            ->where([
                'Cards.name LIKE' => '%//%',
                'Cards.name_jp IS NULL',
            ])
            ->toArray();
//        foreach (array_slice($cards, 0, 5) as $card) {
        foreach ($cards as $card) {
            debug($card['name']);
            $name_exploded = explode(' // ', $card['name']);
            $name_front = $name_exploded[0];
            $name_rear = $name_exploded[1];
            $this->out($name_front);
            $this->out($name_rear);

            // フォーマットを追加
//            $this->createFormatData($card);
        }

        debug(count($cards));

    }
}
