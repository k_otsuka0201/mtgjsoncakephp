<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * CreateSearches shell command.
 */
class CreateFulltextSearchesShell extends Shell
{

    public static $KIND_NAME = 1;
    public static $KIND_NAME_JP = 2;
    public static $KIND_NAME_KANA = 3;
    public static $KIND_FACENAME = 4;

    public static $KIND_TYPE = 11;
    public static $KIND_TYPE_JP = 12;

    public static $KIND_RARITY = 21;

    public static $KIND_COLOR = 31;

    public static $KIND_COLOR_IDENTITY = 41;
    public static $KIND_COLORS = 42;
    public static $KIND_TYPES = 43;
    public static $KIND_SUBTYPES = 44;
    public static $KIND_SUPERTYPES = 45;
    public static $KIND_KEYWORDS = 46;

    public static $KIND_LAYOUT = 51;
    public static $KIND_CODE = 52;
    public static $KIND_BORDER_COLOR = 53;
    public static $KIND_FRAME_VERSION = 54;

    public static $KIND_FORMAT = 61;

    public static $KIND_FLAVOR_TEXT = 71;
    public static $KIND_FLAVOR_TEXT_JP = 72;
    public static $KIND_TEXT = 73;
    public static $KIND_TEXT_JP = 74;

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Attributes');
        $this->loadModel('Cards');
        $this->loadModel('Cardsets');
        $this->loadModel('SearchFulltext');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $connection = ConnectionManager::get('default');
        $connection->execute('TRUNCATE TABLE search_fulltext');

        $cards = $this->Cards->find('all')->toArray();
//        foreach (array_slice($cards, 0, 5) as $card) {
        foreach ($cards as $card) {
            $this->createNameData($card);
            $this->createTypeData($card);
            $this->createFaceNameData($card);
            $this->createNameJpData($card);
            $this->createNameKanaData($card);
            $this->createTypeJpData($card);
            $this->createRarityData($card);
            $this->createColorData($card);
//            $this->createNameMorphologicalAnalysis($card);
//            $this->createTypeMorphologicalAnalysis($card);
//            $this->createFaceNameMorphologicalAnalysis($card);
//            $this->createNameJpMorphologicalAnalysis($card);
//            $this->createTypeJpMorphologicalAnalysis($card);

            foreach (['colorIdentity', 'colors', 'types', 'subtypes', 'supertypes', 'keywords'] as $value) {
                $this->otherMultiAttributes($card, $value);
            }

            foreach (['flavorText', 'text', 'layout', 'code', 'borderColor', 'frameVersion'] as $value) {
                $this->otherSingleAttributes($card, $value);
            }

            foreach (['flavorText_jp', 'text_jp'] as $value) {
                $this->otherSingleAttributesJP($card, $value);
            }

            // フォーマットを追加
            $this->createFormatData($card);
        }

//        debug($cards);
    }

    public function createNameData ($card) {
        /**
         * 検索対象ワードに、半角カンマ、半角ダブルクォート、半角シングルクォートは含めない
         */
        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = str_replace([',', '"', '\'s'], '', $card->name);
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_NAME;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createNameData');
        }
    }

    public function createTypeData ($card) {
        /**
         * 検索対象ワードに、「 —」は含めない
         */
        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = str_replace([' —'], '', $card->type);
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_TYPE;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createTypeData');
        }
    }

    public function createFaceNameData ($card) {
        /**
         * 検索対象ワードに、半角カンマ、半角ダブルクォート、半角シングルクォートは含めない
         */

        if (!$card->faceName) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = str_replace([',', '"', '\'s'], '', $card->faceName);
        $new_entity->card_id = $card->id;
        $new_entity->type = self::$KIND_FACENAME;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createFaceNameData');
        }
    }

    public function createNameJpData ($card) {
        /**
         * 検索対象ワードに、句読点は含めない
         */

        if (!$card->name_jp) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace(['、'], '', $card->name_jp), "c");
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_NAME_JP;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createNameJpData');
        }
    }

    public function createNameKanaData ($card) {
        /**
         * 検索対象ワードに、句読点は含めない
         */

        if (!$card->name_jp) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name_jp = $card->name_kana;
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_NAME_KANA;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createNameKanaData');
        }
    }

    public function createTypeJpData ($card) {
        /**
         * 検索対象ワードに、「 —」は含めない。「・」は半角スペースに変換する
         */

        if (!$card->type_jp) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace([' —', '・'], ['', ' '], $card->type_jp), "c");
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_TYPE_JP;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createTypeJpData');
        }
    }
/*
    public function createNameMorphologicalAnalysis ($card) {

        $name_replaced = str_replace([',', '"', '\'s'], '', $card->name);
        $regist_words = explode(' ', $name_replaced);

        foreach ($regist_words as $regist_word) {
            $new_entity = $this->Searches->newEntity();
            $new_entity->name = $regist_word;
            $new_entity->card_id = $card->id;
            $new_entity->type = 'card_registered';
            $new_entity->offset = mb_strpos($card->name, $regist_word);
            $new_entity->count = mb_substr_count($card->name, $regist_word);
            if ($result = $this->Searches->save($new_entity)) {

            } else {
                $this->abort('Error: createNameJpMorphologicalAnalysis');
            }
        }
    }
*/
/*
    public function createTypeMorphologicalAnalysis ($card) {

        $name_replaced = str_replace([' —'], '', $card->type);
        $regist_words = explode(' ', $name_replaced);

        foreach ($regist_words as $regist_word) {
            $new_entity = $this->Searches->newEntity();
            $new_entity->name = $regist_word;
            $new_entity->card_id = $card->id;
            $new_entity->type = 'type_registered';
            $new_entity->offset = mb_strpos($card->type, $regist_word);
            $new_entity->count = mb_substr_count($card->type, $regist_word);
            if ($result = $this->Searches->save($new_entity)) {

            } else {
                $this->abort('Error: createNameJpMorphologicalAnalysis');
            }
        }
    }
*/
/*
    public function createFaceNameMorphologicalAnalysis ($card) {

        if (!$card->faceName) {
            return false;
        }

        $name_replaced = str_replace([',', '"', '\'s'], '', $card->faceName);
        $regist_words = explode(' ', $name_replaced);

        foreach ($regist_words as $regist_word) {
            $new_entity = $this->Searches->newEntity();
            $new_entity->name = $regist_word;
            $new_entity->card_id = $card->id;
            $new_entity->type = 'facename_registered';
            $new_entity->offset = mb_strpos($card->faceName, $regist_word);
            $new_entity->count = mb_substr_count($card->faceName, $regist_word);
            if ($result = $this->Searches->save($new_entity)) {

            } else {
                $this->abort('Error: createNameJpMorphologicalAnalysis');
            }
        }
    }
*/

//    public function createNameJpMorphologicalAnalysis ($card) {
//        /**
//         * 仮名のオフセットは必要か？
//         */
//
//        if (!$card->name_jp) {
//            return false;
//        }
//
//        $MeCab = new \MeCab\Tagger();
//        $nodes = $MeCab->parseToNode($card->name_jp);
//        $array = [];
//        foreach ($nodes as $n) {
//            $_array = [];
//            $_array['surface'] = $n->getSurface();
//            $_array['feature'] = explode(',', $n->getFeature());
//            $array[] = $_array;
//        }
////        dump($array);
//        $regist_words = [];
//        $regist_kana = [];
//        $words = '';
//        $kanas = '';
//        dump($kanas);
//        for ($i=0; $i<sizeof($array); $i++) {
//            $node = $array[$i];
//            if ($node['feature'][0] === '名詞') {
//                $words.= $node['surface'];
//                $kanas.= isset($node['feature'][7]) ? $node['feature'][7] : '' ;
//                if (in_array($array[$i+1]['feature'][0], ['名詞', '動詞'])) {
//
//                } else {
//                    $regist_words[] = mb_convert_kana($words, "c");
//                    if ($kanas) { $regist_kana[] = mb_convert_kana($kanas, "c"); }
//                    $words = '';
//                    $kanas = '';
//                }
//            } elseif ($node['feature'][0] === '動詞') {
//                $words.= $node['surface'];
//                $kanas.= isset($node['feature'][7]) ? $node['feature'][7] : '' ;
//                if (in_array($array[$i+1]['feature'][0], [])) {
//
//                } else {
//                    $regist_words[] = mb_convert_kana($words, "c");
//                    if ($kanas) { $regist_kana[] = mb_convert_kana($kanas, "c"); }
//                    $words = '';
//                    $kanas = '';
//                }
//            }
//        }
//
//        $this->out($card->name_jp);
//        debug($regist_words);
//        debug($regist_kana);
//
//        foreach ($regist_words as $regist_word) {
//            $new_entity = $this->Searches->newEntity();
//            $new_entity->name = $regist_word;
//            $new_entity->card_id = $card->id;
//            $new_entity->type = 'card_registered';
//            $new_entity->offset = mb_strpos($card->name_jp, $regist_word);
//            $new_entity->count = mb_substr_count($card->name_jp, $regist_word);
//            if ($result = $this->Searches->save($new_entity)) {
//
//            } else {
//                $this->abort('Error: createNameJpMorphologicalAnalysis');
//            }
//        }
//
//        foreach ($regist_kana as $regist_word) {
//            $new_entity = $this->Searches->newEntity();
//            $new_entity->name = $regist_word;
//            $new_entity->card_id = $card->id;
//            $new_entity->type = 'card_registered';
//            $new_entity->offset = 0;
//            $new_entity->count = 1;
//            if ($result = $this->Searches->save($new_entity)) {
//
//            } else {
//                $this->abort('Error: createNameJpMorphologicalAnalysis');
//            }
//        }
//    }
/*
    public function createTypeJpMorphologicalAnalysis ($card) {

        if (!$card->type_jp) {
            return false;
        }

        $regist_words = explode(' ', str_replace([' —', '・'], ['', ' '], $card->type_jp));

        foreach ($regist_words as $regist_word) {
            $new_entity = $this->Searches->newEntity();
            $new_entity->name = mb_convert_kana($regist_word, "c");
            $new_entity->card_id = $card->id;
            $new_entity->type = 'type_jp_registered';
            $new_entity->offset = mb_strpos($card->type_jp, $regist_word);
            $new_entity->count = mb_substr_count($card->type_jp, $regist_word);
            if ($result = $this->Searches->save($new_entity)) {

            } else {
                $this->abort('Error: createTypeJpMorphologicalAnalysis');
            }
        }
    }
*/
    public function createRarityData ($card) {
        if (!$card->rarity) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = $card->rarity;
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_RARITY;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createRarityData');
        }
    }

    public function createColorData ($card) {
        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = $card->color;
        $new_entity->card_id = $card->id;
        $new_entity->kind = self::$KIND_COLOR;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createColorData');
        }
    }

    public function createFormatData ($card) {
        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = 'Standard';
        $new_entity->card_id = $card->id;
        $new_entity->type = self::$KIND_FORMAT;
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: createFormatData');
        }
    }

    public function otherMultiAttributes ($card, $attr_name) {
        $attr_value = $card->$attr_name;
        if (!$attr_value) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = str_replace(',', ' ', $attr_value);
        $new_entity->card_id = $card->id;
        $new_entity->kind = $this->convertKind($attr_name);
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: otherMultiAttributes');
        }

    }

    public function otherSingleAttributes ($card, $attr_name) {
        $attr_value = $card->$attr_name;
        if (!$attr_value) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name = str_replace(',', ' ', $attr_value);
        $new_entity->card_id = $card->id;
        $new_entity->kind = $this->convertKind($attr_name);
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: otherSingleAttributes');
        }

    }

    public function otherSingleAttributesJP ($card, $attr_name) {
        $attr_value = $card->$attr_name;
        if (!$attr_value) {
            return false;
        }

        $new_entity = $this->SearchFulltext->newEntity();
        $new_entity->name_jp = str_replace(',', ' ', $attr_value);
        $new_entity->card_id = $card->id;
        $new_entity->kind = $this->convertKind($attr_name);
        if (!$result = $this->SearchFulltext->save($new_entity)) {
            $this->abort('Error: otherSingleAttributes');
        }

    }

    public function convertKind ($attr_name) {

        $screaming_snake_case = ltrim(strtoupper(preg_replace('/[A-Z]/', '_\0', $attr_name)), '_');
        $attr_variable = 'KIND_' . $screaming_snake_case;
        $kind_id = self::$$attr_variable;

        return $kind_id;
    }


}
