<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * CreateSearches shell command.
 */
class CreateKeywordSearchesShell extends Shell
{
    public static $CATEGORY_CARD = 0;

    public static $KIND_NAME = 1;
    public static $KIND_NAME_JP = 2;
    public static $KIND_NAME_KANA = 3;
    public static $KIND_FACENAME = 4;

    public static $KIND_TYPE = 11;
    public static $KIND_TYPE_JP = 12;

    public static $KIND_RARITY = 21;

    public static $KIND_COLOR = 31;

    public static $KIND_COLOR_IDENTITY = 41;
    public static $KIND_COLORS = 42;
    public static $KIND_TYPES = 43;
    public static $KIND_SUBTYPES = 44;
    public static $KIND_SUPERTYPES = 45;
    public static $KIND_KEYWORDS = 46;

    public static $KIND_LAYOUT = 51;
    public static $KIND_CODE = 52;
    public static $KIND_BORDER_COLOR = 53;
    public static $KIND_FRAME_VERSION = 54;

    public static $KIND_FORMAT = 61;

    public static $KIND_FLAVOR_TEXT = 71;
    public static $KIND_FLAVOR_TEXT_JP = 72;
    public static $KIND_TEXT = 73;
    public static $KIND_TEXT_JP = 74;

    public static $CATEGORY_CARDSET = 100;

    public static $KIND_CARDSET_NAME = 101;
    public static $KIND_CARDSET_NAMEJP = 102;
    public static $KIND_CARDSET_CODE = 103;
    public static $KIND_CARDSET_NAMEKANA = 104;

    public static $CATEGORY_RARITY = 200;

    public static $KIND_RARITY_NAME = 201;
    public static $KIND_RARITY_NAMEJP = 202;
    public static $KIND_RARITY_CODE = 203;

    public static $CATEGORY_COLOR = 300;

    public static $KIND_COLOR_NAME = 301;
    public static $KIND_COLOR_NAMEJP = 302;
    public static $KIND_COLOR_CODE = 303;

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Attributes');
        $this->loadModel('Cards');
        $this->loadModel('Cardsets');
        $this->loadModel('SearchKeywords');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $connection = ConnectionManager::get('default');
        $connection->execute('TRUNCATE TABLE search_keywords');

        $cards = $this->Cards->find('all')->toArray();
//        foreach (array_slice($cards, 0, 5) as $card) {
        foreach ($cards as $card) {
            $this->createNameData($card);
            $this->createTypeData($card);
            $this->createFaceNameData($card);
            $this->createNameJpData($card);
            $this->createNameKanaData($card);
            $this->createTypeJpData($card);
            $this->createRarityData($card);
            $this->createColorData($card);

            $this->createFormatData($card);

            foreach (['colorIdentity', 'colors', 'types', 'subtypes', 'supertypes', 'keywords'] as $value) {
                $this->otherMultiAttributes($card, $value);
            }

            foreach (['flavorText', 'text', 'layout', 'code', 'borderColor', 'frameVersion'] as $value) {
                $this->otherSingleAttributes($card, $value);
            }

            foreach (['flavorText_jp', 'text_jp'] as $value) {
                $this->otherSingleAttributesJP($card, $value);
            }
        }

        $cardsets = $this->Cardsets->find('all')->toArray();
//        foreach (array_slice($cardsets, 0, 5) as $cardset) {
        foreach ($cardsets as $cardset) {
            $this->createCardsetName($cardset);
            $this->createCardsetNameJp($cardset);
            $this->createCardsetCode($cardset);
            $this->createCardsetNameKana($cardset);
        }

        $attributes = $this->Attributes->find('all')->where(['type IN' => ['color', 'rarity']])->toArray();
//        foreach (array_slice($attributes, 0, 5) as $attribute) {
        foreach ($attributes as $attribute) {
            $this->createAttributeName($attribute);
            $this->createAttributeNameJp($attribute);
            $this->createAttributeCode($attribute);
        }

//        debug($cards);
    }

    public function createNameData ($card) {
        /**
         * 検索対象ワードに、半角カンマ、半角ダブルクォート、半角シングルクォートは含めない
         */
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = str_replace([',', '"', '\'s'], '', $card->name);
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_NAME;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createNameData');
        }
    }

    public function createCardsetName ($cardset) {
        /**
         * 検索対象ワードに、半角カンマ、半角ダブルクォート、半角シングルクォートは含めない
         */
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = str_replace([',', '"', '\'s'], '', $cardset->name);
        $new_entity->category = self::$CATEGORY_CARDSET;
        $new_entity->kind = self::$KIND_CARDSET_NAME;
        $new_entity->convert_id = $cardset->id;
        $new_entity->convert_name = $cardset->name;
        $new_entity->convert_name_jp = $cardset->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createCardsetName');
        }
    }

    public function createCardsetCode ($cardset) {
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = $cardset->code;
        $new_entity->category = self::$CATEGORY_CARDSET;
        $new_entity->kind = self::$KIND_CARDSET_CODE;
        $new_entity->convert_id = $cardset->id;
        $new_entity->convert_name = $cardset->name;
        $new_entity->convert_name_jp = $cardset->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createCardsetCode');
        }
    }

    public function createAttributeName ($attribute) {
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = $attribute->name;
        $category_variable = 'CATEGORY_' . strtoupper($attribute->type);
        $new_entity->category = self::$$category_variable;
        $attr_variable = 'KIND_' . strtoupper($attribute->type) . '_NAME';
        $new_entity->kind = self::$$attr_variable;
        $new_entity->convert_id = $attribute->id;
        $new_entity->convert_name = $attribute->name;
        $new_entity->convert_name_jp = $attribute->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createAttributeName');
        }
    }

    public function createAttributeCode ($attribute) {
        if (!$attribute->code) {
            return false;
        }
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = $attribute->code;
        $category_variable = 'CATEGORY_' . strtoupper($attribute->type);
        $new_entity->category = self::$$category_variable;
        $attr_variable = 'KIND_' . strtoupper($attribute->type) . '_CODE';
        $new_entity->kind = self::$$attr_variable;
        $new_entity->convert_id = $attribute->id;
        $new_entity->convert_name = $attribute->name;
        $new_entity->convert_name_jp = $attribute->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createAttributeCode');
        }
    }

    public function createTypeData ($card) {
        /**
         * 検索対象ワードに、「 —」は含めない
         */
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = str_replace([' —'], '', $card->type);
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_TYPE;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createTypeData');
        }
    }

    public function createFaceNameData ($card) {
        /**
         * 検索対象ワードに、半角カンマ、半角ダブルクォート、半角シングルクォートは含めない
         */

        if (!$card->faceName) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = str_replace([',', '"', '\'s'], '', $card->faceName);
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_FACENAME;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createFaceNameData');
        }
    }

    public function createNameJpData ($card) {
        /**
         * 検索対象ワードに、句読点は含めない
         */

        if (!$card->name_jp) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace(['、'], '', $card->name_jp), "c");
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_NAME_JP;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createNameJpData');
        }
    }

    public function createNameKanaData ($card) {
        /**
         * 検索対象ワードに、句読点は含めない
         */

        if (!$card->name_jp) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace(['、'], '', $card->name_kana), "c");
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_NAME_KANA;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createNameKanaData');
        }
    }

    public function createCardsetNameJp ($cardset) {
        /**
         * 検索対象ワードに、句読点は含めない
         */

        if (!$cardset->name_jp) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace(['、'], '', $cardset->name_jp), "c");
        $new_entity->category = self::$CATEGORY_CARDSET;
        $new_entity->kind = self::$KIND_CARDSET_NAMEJP;
        $new_entity->convert_id = $cardset->id;
        $new_entity->convert_name = $cardset->name;
        $new_entity->convert_name_jp = $cardset->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createCardsetNameJp');
        }
    }

    public function createCardsetNameKana ($cardset) {
        /**
         * 検索対象ワードに、句読点は含めない
         */

        if (!$cardset->name_kana) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace(['、'], '', $cardset->name_kana), "c");
        $new_entity->category = self::$CATEGORY_CARDSET;
        $new_entity->kind = self::$KIND_CARDSET_NAMEKANA;
        $new_entity->convert_id = $cardset->id;
        $new_entity->convert_name = $cardset->name;
        $new_entity->convert_name_jp = $cardset->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createCardsetNameJp');
        }
    }

    public function createAttributeNameJp ($attribute) {

        if (!$attribute->name_jp) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace(['、'], '', $attribute->name_kana), "c");
        $category_variable = 'CATEGORY_' . strtoupper($attribute->type);
        $new_entity->category = self::$$category_variable;
        $attr_variable = 'KIND_' . strtoupper($attribute->type) . '_NAMEJP';
        $new_entity->kind = self::$$attr_variable;
        $new_entity->convert_id = $attribute->id;
        $new_entity->convert_name = $attribute->name;
        $new_entity->convert_name_jp = $attribute->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createAttributeNameJp');
        }
    }

    public function createTypeJpData ($card) {
        /**
         * 検索対象ワードに、「 —」は含めない。「・」は半角スペースに変換する
         */

        if (!$card->type_jp) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = mb_convert_kana(str_replace([' —', '・'], ['', ' '], $card->type_jp), "c");
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_TYPE_JP;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createTypeJpData');
        }
    }

    public function createRarityData ($card) {
        if (!$card->rarity) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = $card->rarity;
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_RARITY;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createRarityData');
        }
    }

    public function createColorData ($card) {
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = $card->color;
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_COLOR;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createColorData');
        }
    }

    public function createFormatData ($card) {
        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = 'Standard';
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = self::$KIND_FORMAT;
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: createFormatData');
        }
    }

    public function otherMultiAttributes ($card, $attr_name) {
        $attr_value = $card->$attr_name;
        if (!$attr_value) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = str_replace(',', ' ', $attr_value);
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = $this->convertKind($attr_name);
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: otherMultiAttributes');
        }

    }

    public function otherSingleAttributes ($card, $attr_name) {
        $attr_value = $card->$attr_name;
        if (!$attr_value) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name = str_replace(',', ' ', $attr_value);
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = $this->convertKind($attr_name);
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: otherSingleAttributes');
        }

    }

    public function otherSingleAttributesJP ($card, $attr_name) {
        $attr_value = $card->$attr_name;
        if (!$attr_value) {
            return false;
        }

        $new_entity = $this->SearchKeywords->newEntity();
        $new_entity->name_jp = str_replace(',', ' ', $attr_value);
        $new_entity->category = self::$CATEGORY_CARD;
        $new_entity->kind = $this->convertKind($attr_name);
        $new_entity->convert_id = $card->id;
        $new_entity->convert_name = $card->name;
        $new_entity->convert_name_jp = $card->name_jp;
        if (!$result = $this->SearchKeywords->save($new_entity)) {
            $this->abort('Error: otherSingleAttributes');
        }

    }

    public function convertKind ($attr_name) {

        $screaming_snake_case = ltrim(strtoupper(preg_replace('/[A-Z]/', '_\0', $attr_name)), '_');
        $attr_variable = 'KIND_' . $screaming_snake_case;
        $kind_id = self::$$attr_variable;

        return $kind_id;
    }


}
