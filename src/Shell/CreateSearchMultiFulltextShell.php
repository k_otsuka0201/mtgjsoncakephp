<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * CreateSearches shell command.
 */
class CreateSearchMultiFulltextShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Attributes');
        $this->loadModel('Cards');
        $this->loadModel('Cardsets');
        $this->loadModel('SearchMultiFulltext');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $connection = ConnectionManager::get('default');
        $connection->execute('TRUNCATE TABLE search_multi_fulltext');

        $cards = $this->Cards->find('all')->toArray();
//        foreach (array_slice($cards, 0, 5) as $card) {
        foreach ($cards as $card) {

            $data = [];
            $data['name'] = $card->name;
            $data['name'].= ' ' . $card->rarity;
            $data['name'].= ' ' . $card->rarity;
            $data['name'].= ' ' . $card->flavorText;
            $data['name'].= ' ' . $card->text;
            $data['name'].= ' ' . $card->type;
            $data['name'].= ' ' . $card->types;
            $data['name'].= ' ' . $card->layout;
            $data['name'].= ' ' . $card->subtypes;
            $data['name'].= isset($card->supertypes) ? ' ' . $card->supertypes : '' ;
            $data['name'].= ' ' . $card->code;
            $data['name'].= ' ' . $card->borderColor;
            $data['name'].= ' ' . $card->frameVersion;
            $data['name'].= isset($card->keywords) ? ' ' . $card->keywords : '' ;

            $data['name_jp'] = isset($card->name_jp) ? $card->name_jp : '' ;
            $data['name_jp'].= isset($card->name_kana) ? ' ' . $card->name_kana : '' ;
            $data['name_jp'].= isset($card->flavorText_jp) ? ' ' . $card->flavorText_jp : '' ;
            $data['name_jp'].= isset($card->text_jp) ? ' ' . $card->text_jp : '' ;
            $data['name_jp'].= isset($card->type_jp) ? ' ' . $card->type_jp : '' ;

            $new_entity = $this->SearchMultiFulltext->newEntity();
            $new_entity->name = $data['name'];
            $new_entity->name_jp = $data['name_jp'];
            $new_entity->card_id = $card->id;
            if (!$result = $this->SearchMultiFulltext->save($new_entity)) {
                $this->abort('Error: main');
            }
        }

//        debug($cards);
    }


}
