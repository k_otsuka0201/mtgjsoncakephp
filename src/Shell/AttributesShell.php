<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * Attributes shell command.
 */
class AttributesShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     *
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('Attributes');
        $this->loadModel('AttributesConvert');
        $this->loadModel('Cards');
        $this->loadModel('CardsetsConvert');
        $this->loadModel('Cardsets');
        $this->loadModel('Searches');
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $connection = ConnectionManager::get('default');
        $connection->execute('TRUNCATE TABLE attributes_convert');

        $attributes = $this->Attributes->find('all')->toArray();
//        foreach (array_slice($attributes, 0, 5) as $attribute) {
        foreach ($attributes as $attribute) {
            $name = $attribute->name;

            if ($attribute->name_jp) {
                $name_jp = $attribute->name_jp;
                $this->createConvertData($name_jp, $name);
            }

            if ($attribute->name_kana) {
                $name_kana = $attribute->name_kana;
                $this->createConvertData($name_kana, $name);
            }

            if ($attribute->code) {
                $code = $attribute->code;
                $this->createConvertData($code, $name);
            }

        }

//        debug($cards);
    }

    public function createConvertData ($name, $code) {
        $new_entity = $this->AttributesConvert->newEntity();
        $new_entity->name = $name;
        $new_entity->name_converted = $code;
        if ($result = $this->AttributesConvert->save($new_entity)) {
            return $result->id;
        } else {
            $this->abort('Error: createConvertData');
        }
    }
}
