<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConvertCardsets Model
 *
 * @method \App\Model\Entity\ConvertCardset get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConvertCardset newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConvertCardset[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConvertCardset|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConvertCardset|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConvertCardset patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConvertCardset[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConvertCardset findOrCreate($search, callable $callback = null, $options = [])
 */
class ConvertCardsetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('convert_cardsets');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('name_converted')
            ->maxLength('name_converted', 64)
            ->allowEmpty('name_converted');

        return $validator;
    }
}
