<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cardsets Model
 *
 * @property \App\Model\Table\CardsBigwebTable|\Cake\ORM\Association\HasMany $CardsBigweb
 *
 * @method \App\Model\Entity\Cardset get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cardset newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cardset[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cardset|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cardset|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cardset patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cardset[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cardset findOrCreate($search, callable $callback = null, $options = [])
 */
class CardsetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cardsets');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('CardsBigweb', [
            'foreignKey' => 'cardset_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        $validator
            ->scalar('name_jp')
            ->maxLength('name_jp', 255)
            ->allowEmpty('name_jp');

        $validator
            ->scalar('name_kana')
            ->maxLength('name_kana', 255)
            ->allowEmpty('name_kana');

        $validator
            ->scalar('type')
            ->maxLength('type', 64)
            ->allowEmpty('type');

        $validator
            ->scalar('block')
            ->maxLength('block', 64)
            ->allowEmpty('block');

        $validator
            ->scalar('code')
            ->maxLength('code', 8)
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->scalar('format')
            ->maxLength('format', 64)
            ->allowEmpty('format');

        $validator
            ->integer('baseSetSize')
            ->requirePresence('baseSetSize', 'create')
            ->notEmpty('baseSetSize');

        $validator
            ->integer('totalSetSize')
            ->requirePresence('totalSetSize', 'create')
            ->notEmpty('totalSetSize');

        $validator
            ->boolean('isFoilOnly')
            ->requirePresence('isFoilOnly', 'create')
            ->notEmpty('isFoilOnly');

        $validator
            ->dateTime('release_date')
            ->allowEmpty('release_date');

        return $validator;
    }
}
