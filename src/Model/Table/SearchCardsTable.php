<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SearchCards Model
 *
 * @property \App\Model\Table\CardsTable|\Cake\ORM\Association\BelongsTo $Cards
 *
 * @method \App\Model\Entity\SearchCard get($primaryKey, $options = [])
 * @method \App\Model\Entity\SearchCard newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SearchCard[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SearchCard|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchCard|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchCard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SearchCard[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SearchCard findOrCreate($search, callable $callback = null, $options = [])
 */
class SearchCardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('search_cards');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cards', [
            'foreignKey' => 'card_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmpty('type');

        $validator
            ->integer('offset')
            ->requirePresence('offset', 'create')
            ->notEmpty('offset');

        $validator
            ->integer('count')
            ->requirePresence('count', 'create')
            ->notEmpty('count');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['card_id'], 'Cards'));

        return $rules;
    }

    /**
     * 1つの検索ワードで、SearchCards.nameに一致する検索データを取得
     *
     * @param String $word
     * @return Query
     */
    public function getDataByName (String $word)
    {
        return $this->find('all')
            ->where([
                'name' => $word,
            ]);
    }

    /**
     * 複数の検索ワードで、SearchCards.nameに一致する検索データを取得
     *
     * @param array $word
     * @return Query
     */
    public function getDataByNames (Array $word)
    {
        return $this->find('all')
            ->where([
                'name IN' => $word,
            ]);
    }

}
