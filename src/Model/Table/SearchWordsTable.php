<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SearchWords Model
 *
 * @property \App\Model\Table\CardsetsTable|\Cake\ORM\Association\BelongsTo $Cardsets
 *
 * @method \App\Model\Entity\SearchWord get($primaryKey, $options = [])
 * @method \App\Model\Entity\SearchWord newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SearchWord[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SearchWord|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchWord|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchWord patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SearchWord[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SearchWord findOrCreate($search, callable $callback = null, $options = [])
 */
class SearchWordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('search_words');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cardsets', [
            'foreignKey' => 'cardset_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('fullname')
            ->maxLength('fullname', 255)
            ->requirePresence('fullname', 'create')
            ->notEmpty('fullname');

        $validator
            ->integer('next')
            ->allowEmpty('next');

        $validator
            ->boolean('is_terminal')
            ->requirePresence('is_terminal', 'create')
            ->notEmpty('is_terminal');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cardset_id'], 'Cardsets'));

        return $rules;
    }
}
