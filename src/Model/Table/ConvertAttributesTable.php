<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConvertAttributes Model
 *
 * @method \App\Model\Entity\ConvertAttribute get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConvertAttribute newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConvertAttribute[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConvertAttribute|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConvertAttribute|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConvertAttribute patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConvertAttribute[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConvertAttribute findOrCreate($search, callable $callback = null, $options = [])
 */
class ConvertAttributesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('convert_attributes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('name_converted')
            ->maxLength('name_converted', 64)
            ->allowEmpty('name_converted');

        return $validator;
    }

    /**
     * 1つの検索ワードで、ConvertAttributes.nameに一致する検索データを取得
     *
     * @param String $word
     * @return Query
     */
    public function getDataByName (String $word)
    {
        return $this->find('all')
            ->where([
                'name' => $word,
            ]);
    }

    /**
     * 複数の検索ワードで、ConvertAttributes.nameに一致する検索データを取得
     *
     * @param array $word
     * @return Query
     */
    public function getDataByNames (Array $word)
    {
        return $this->find('all')
            ->where([
                'name IN' => $word,
            ]);
    }
}
