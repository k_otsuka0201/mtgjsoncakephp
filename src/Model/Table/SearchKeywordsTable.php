<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SearchKeywords Model
 *
 * @method \App\Model\Entity\SearchKeyword get($primaryKey, $options = [])
 * @method \App\Model\Entity\SearchKeyword newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SearchKeyword[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SearchKeyword|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchKeyword|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchKeyword patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SearchKeyword[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SearchKeyword findOrCreate($search, callable $callback = null, $options = [])
 */
class SearchKeywordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('search_keywords');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('name_jp')
            ->allowEmpty('name_jp');

        $validator
            ->integer('kind')
            ->requirePresence('kind', 'create')
            ->notEmpty('kind');

        return $validator;
    }
}
