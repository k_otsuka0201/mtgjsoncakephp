<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cards Model
 *
 * @property \App\Model\Table\SearchCardsTable|\Cake\ORM\Association\HasMany $SearchCards
 *
 * @method \App\Model\Entity\Card get($primaryKey, $options = [])
 * @method \App\Model\Entity\Card newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Card[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Card|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Card|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Card patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Card[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Card findOrCreate($search, callable $callback = null, $options = [])
 */
class CardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cards');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('SearchCards', [
            'foreignKey' => 'card_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        $validator
            ->scalar('name_jp')
            ->maxLength('name_jp', 255)
            ->allowEmpty('name_jp');

        $validator
            ->scalar('name_kana')
            ->maxLength('name_kana', 255)
            ->allowEmpty('name_kana');

        $validator
            ->scalar('rarity')
            ->maxLength('rarity', 32)
            ->allowEmpty('rarity');

        $validator
            ->scalar('color')
            ->maxLength('color', 255)
            ->allowEmpty('color');

        $validator
            ->scalar('colorIdentity')
            ->maxLength('colorIdentity', 255)
            ->allowEmpty('colorIdentity');

        $validator
            ->scalar('colors')
            ->maxLength('colors', 255)
            ->allowEmpty('colors');

        $validator
            ->scalar('flavorText')
            ->maxLength('flavorText', 255)
            ->allowEmpty('flavorText');

        $validator
            ->scalar('flavorText_jp')
            ->maxLength('flavorText_jp', 255)
            ->allowEmpty('flavorText_jp');

        $validator
            ->scalar('text')
            ->allowEmpty('text');

        $validator
            ->scalar('text_jp')
            ->allowEmpty('text_jp');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmpty('type');

        $validator
            ->scalar('type_jp')
            ->maxLength('type_jp', 255)
            ->allowEmpty('type_jp');

        $validator
            ->scalar('types')
            ->maxLength('types', 255)
            ->allowEmpty('types');

        $validator
            ->scalar('layout')
            ->maxLength('layout', 64)
            ->allowEmpty('layout');

        $validator
            ->scalar('subtypes')
            ->maxLength('subtypes', 255)
            ->allowEmpty('subtypes');

        $validator
            ->scalar('supertypes')
            ->maxLength('supertypes', 255)
            ->allowEmpty('supertypes');

        $validator
            ->scalar('code')
            ->maxLength('code', 64)
            ->allowEmpty('code');

        $validator
            ->scalar('borderColor')
            ->maxLength('borderColor', 64)
            ->allowEmpty('borderColor');

        $validator
            ->scalar('frameVersion')
            ->maxLength('frameVersion', 64)
            ->allowEmpty('frameVersion');

        $validator
            ->scalar('keywords')
            ->maxLength('keywords', 255)
            ->allowEmpty('keywords');

        $validator
            ->scalar('faceName')
            ->maxLength('faceName', 255)
            ->allowEmpty('faceName');

        $validator
            ->scalar('side')
            ->maxLength('side', 8)
            ->allowEmpty('side');

        return $validator;
    }

    /**
     * 複数のCards.idに一致するカードデータを取得
     *
     * @param $card_ids
     * @return Query
     */
    public function getDataByIds ($card_ids)
    {
        return $this->find('all')
            ->where([
                'id IN' => $card_ids,
            ]);
    }

}
