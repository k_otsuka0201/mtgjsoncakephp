<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Card Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_jp
 * @property string $name_kana
 * @property string $rarity
 * @property string $color
 * @property string $colorIdentity
 * @property string $colors
 * @property string $flavorText
 * @property string $flavorText_jp
 * @property string $text
 * @property string $text_jp
 * @property string $type
 * @property string $type_jp
 * @property string $types
 * @property string $layout
 * @property string $subtypes
 * @property string $supertypes
 * @property string $code
 * @property string $borderColor
 * @property string $frameVersion
 * @property string $keywords
 * @property string $faceName
 * @property string $side
 *
 * @property \App\Model\Entity\SearchCard[] $search_cards
 */
class Card extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_jp' => true,
        'name_kana' => true,
        'rarity' => true,
        'color' => true,
        'colorIdentity' => true,
        'colors' => true,
        'flavorText' => true,
        'flavorText_jp' => true,
        'text' => true,
        'text_jp' => true,
        'type' => true,
        'type_jp' => true,
        'types' => true,
        'layout' => true,
        'subtypes' => true,
        'supertypes' => true,
        'code' => true,
        'borderColor' => true,
        'frameVersion' => true,
        'keywords' => true,
        'faceName' => true,
        'side' => true,
        'search_cards' => true
    ];
}
