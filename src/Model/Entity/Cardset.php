<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cardset Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_jp
 * @property string $name_kana
 * @property string $type
 * @property string $block
 * @property string $code
 * @property string $format
 * @property int $baseSetSize
 * @property int $totalSetSize
 * @property bool $isFoilOnly
 * @property \Cake\I18n\FrozenTime $release_date
 *
 * @property \App\Model\Entity\CardsBigweb[] $cards_bigweb
 */
class Cardset extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_jp' => true,
        'name_kana' => true,
        'type' => true,
        'block' => true,
        'code' => true,
        'format' => true,
        'baseSetSize' => true,
        'totalSetSize' => true,
        'isFoilOnly' => true,
        'release_date' => true,
        'cards_bigweb' => true
    ];
}
