<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SearchMultiFulltext Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_jp
 * @property int $card_id
 *
 * @property \App\Model\Entity\Card $card
 */
class SearchMultiFulltext extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_jp' => true,
        'card_id' => true,
        'card' => true
    ];
}
