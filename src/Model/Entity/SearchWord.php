<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SearchWord Entity
 *
 * @property int $id
 * @property string $name
 * @property string $fullname
 * @property int $cardset_id
 * @property int $next
 * @property bool $is_terminal
 *
 * @property \App\Model\Entity\Cardset $cardset
 */
class SearchWord extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'fullname' => true,
        'cardset_id' => true,
        'next' => true,
        'is_terminal' => true,
        'cardset' => true
    ];
}
