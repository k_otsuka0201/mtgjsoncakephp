<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * FullTextAnalysis component
 */
class FullTextAnalysisComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    protected $searchwords = null;
    protected $analyzed = [];
    protected $group_analyzed = [];
    protected $sequence = [];
    protected $group_sequence = [];
    protected $cnt = 0;

    public function KeywordAnalysisExecute ($search_data)
    {
        $this->searchwords = $search_data['keywords'];
        $this->KeywordAnalysis($this->searchwords[$this->cnt]);

        return $this->group_analyzed;
    }

    public function KeywordAnalysis ($search_data)
    {
        $this->sequence[] = $this->searchwords[$this->cnt];
//        $sequence_imploded = implode(' ', $this->sequence);
        if (count($this->searchwords) - 1 >= $this->cnt) {
            $result_has_keywords = $this->hasKeywords($this->sequence);
            if ($result_has_keywords) {
                $this->cnt+= 1;
            } else {
                if (count($this->sequence) <= 1) {
                    // sequenceが1の場合は無限ループするのでanalyzedに渡したら、カウントをプラスする
                    $this->analyzed[] = implode(' ', $this->sequence);
                    $this->group_analyzed[] = [
                        'sequence' => $this->sequence,
                        'groups' => [
                            [
                                'category' => null,
                                'kind' => null,
                                'cnt' => 0,
                            ]
                        ],
                    ];
                    $this->cnt+= 1;
                } else {
                    array_pop($this->sequence);
                    $this->analyzed[] = implode(' ', $this->sequence);
                    $this->group_analyzed[] = [
                        'sequence' => $this->sequence,
                        'groups' => $this->group_sequence,
                    ];
                    Log::error('KeywordAnalysis Process');
                    Log::error($this->group_analyzed);
                }
                $this->sequence = [];
                $this->group_sequence = [];
            }
            if (isset($this->searchwords[$this->cnt])) {
                $this->group_sequence = $result_has_keywords;
                $this->KeywordAnalysis($this->searchwords[$this->cnt]);
            } else {
                $this->analyzed[] = implode(' ', $this->sequence);
                $this->group_analyzed[] = [
                    'sequence' => $this->sequence,
                    'groups' => $result_has_keywords,
                ];
                Log::error('KeywordAnalysis End');
                Log::error($this->group_analyzed);
            }
        }
    }

    public function hasKeywords ($keyword_array)
    {
        $keywords_prefixed = [];
        foreach ($keyword_array as $keyword) {
            $keywords_prefixed[] = '+' . $keyword;
        }

        $sql_keywords = implode(' ', $keywords_prefixed);
        $conn = ConnectionManager::get('default');
        $sql = <<<EOT
SELECT category, kind, count(*) AS cnt
 FROM search_keywords
 WHERE MATCH(name_jp) AGAINST ('{$sql_keywords}' IN BOOLEAN MODE)
 OR MATCH(name) AGAINST ('{$sql_keywords}' IN BOOLEAN MODE)
 GROUP BY category, kind
EOT;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $search_fulltexts = $stmt->fetchAll('assoc');
//        Log::error('$sql_keywords');
//        Log::error($sql_keywords);
        Log::error('$sql');
        Log::error($sql);
//        Log::error($search_fulltexts);
        if ($search_fulltexts) {
            return $search_fulltexts;
        } else {
            return false;
        }
    }

}
