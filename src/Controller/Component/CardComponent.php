<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Log\Log;

/**
 * Card component
 */
class CardComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param array $config
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    public function getCardsByAnalyzed ($group_analyzed)
    {
        $filtered_data = [];
        foreach ($group_analyzed as $analyzed) {
            $sequences = $analyzed['sequence'];
            $group = $analyzed['groups'][0];
            $kind = $group['kind'];
            foreach ($sequences as $sequence) {
                switch ($kind) {
                    case 1:
                        $filtered_data[] = [
                            'name LIKE' => "%{$sequence}%",
                        ];
                        break;
                    case 2:
                        $filtered_data[] = [
                            'name_jp LIKE' => "%{$sequence}%",
                        ];
                        break;
                    case 3:
                        $filtered_data[] = [
                            'name_kana LIKE' => "%{$sequence}%",
                        ];
                        break;
                    case 21:
                        $filtered_data[] = [
                            'rarity LIKE' => "%{$sequence}%",
                        ];
                        break;
                    case 31:
                        $filtered_data[] = [
                            'color LIKE' => "%{$sequence}%",
                        ];
                        break;
                    case 52:
                        $filtered_data[] = [
                            'code' => "{$sequence}",
                        ];
                        break;
                }
            }
        }

        $cards = $this->controller->Cards->find('all')->where($filtered_data);

        return $cards;
    }

    public function getCardsByProperties ($request_data) {
        $card_data = [];
        $card_ids_uniqued = $this->group_analyzed;

        $kind = (int)$request_data['kind'];
        $sequence = $request_data['sequence'];
        switch ($kind) {
            case 1:
                $clause = [
                    'name LIKE' => "%{$sequence}%",
                ];
                break;
            case 2:
                $clause = [
                    'name_jp LIKE' => "%{$sequence}%",
                ];
                break;
            case 3:
                $clause = [
                    'name_kana LIKE' => "%{$sequence}%",
                ];
                break;
            case 21:
                $clause = [
                    'rarity LIKE' => "%{$sequence}%",
                ];
                break;
            case 31:
                $clause = [
                    'color LIKE' => "%{$sequence}%",
                ];
                break;
            case 52:
                $clause = [
                    'code' => $sequence,
                ];
                break;
        }

        $card_data = $this->controller->Cards->find('all')->where($clause)->limit(10)->toArray();

        return [
            'card_data' => $card_data,
            'total_count' => count($card_data),
        ];
    }

    public function getCardsByIds ($card_ids)
    {
        $card_data = [];
        $cards = $this->controller->Cards->getDataByIds($card_ids)->toArray();
        if ($cards) {
            foreach ($cards as $card) {
                $card_data[$card->id] = $card;
            }
        }
        return [
            'card_data' => $card_data,
            'total_count' => count($cards),
        ];
    }

    protected function getCards ()
    {

    }
}
