<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * KeywordAnalysis component
 */
class KeywordAnalysisComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param array $config
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    public function getRequestGetData ()
    {
        $keywords = $this->controller->request->getQuery('keywords');
        $page = $this->controller->request->getQuery('page');
        $request_data = [
            'keywords' => $keywords,
            'page' => $page,
        ];
        return $request_data;
    }

    public function getRequestPostData ()
    {
        $keywords = $this->controller->request->getData('keywords');
        $request_data = [
            'keywords' => $keywords,
            'page' => $this->controller->request->getData('page'),
            'sequence' => $this->controller->request->getData('sequence'),
            'category' => $this->controller->request->getData('category'),
            'kind' => $this->controller->request->getData('kind'),
            'convert_id' => $this->controller->request->getData('convert_id'),
        ];
        return $request_data;
    }

    /**
     * 全文検索向けの入力キーワードの処理
     *
     * @param $keywords
     * @return array|false|string[]
     */
    public function getSearchData ($request_data)
    {
        $search_data = [];
        $keywords = $request_data['keywords'];

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data['keywords'] = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        return $search_data;
    }

    protected $searchwords = null;
    protected $analyzed = [];
    protected $group_analyzed = [];
    protected $sequence = [];
    protected $group_sequence = [];
    protected $cnt = 0;

    public function KeywordAnalysis ($search_data)
    {
        $this->sequence[] = $this->searchwords[$this->cnt];
//        $sequence_imploded = implode(' ', $this->sequence);
        if (count($this->searchwords) - 1 >= $this->cnt) {
            $result_has_keywords = $this->hasKeywords($this->sequence);
            if ($result_has_keywords) {
                $this->cnt+= 1;
            } else {
                if (count($this->sequence) <= 1) {
                    // sequenceが1の場合は無限ループするのでanalyzedに渡したら、カウントをプラスする
                    $this->analyzed[] = implode(' ', $this->sequence);
                    $this->group_analyzed[] = [
                        'sequence' => $this->sequence,
                        'groups' => [
                            [
                                'category' => null,
                                'kind' => null,
                                'cnt' => 0,
                                'convert_id' => 0,
                                'convert_name' => null,
                                'convert_name_jp' => null,
                            ]
                        ],
                    ];
                    $this->cnt+= 1;
                } else {
                    array_pop($this->sequence);
                    $this->analyzed[] = implode(' ', $this->sequence);
                    $this->group_analyzed[] = [
                        'sequence' => $this->sequence,
                        'groups' => $this->group_sequence,
                    ];
                    Log::error('KeywordAnalysis Process');
                    Log::error($this->group_analyzed);
                }
                $this->sequence = [];
                $this->group_sequence = [];
            }
            if (isset($this->searchwords[$this->cnt])) {
                $this->group_sequence = $result_has_keywords;
                $this->KeywordAnalysis($this->searchwords[$this->cnt]);
            } else {
                $this->analyzed[] = implode(' ', $this->sequence);
                $this->group_analyzed[] = [
                    'sequence' => $this->sequence,
                    'groups' => $result_has_keywords,
                ];
                Log::error('KeywordAnalysis End');
                Log::error($this->group_analyzed);
            }
        }
    }

    public function hasKeywords ($keyword_array)
    {
        $keywords_prefixed = [];
        foreach ($keyword_array as $keyword) {
            $keywords_prefixed[] = '+' . $keyword;
        }

        $sql_keywords = implode(' ', $keywords_prefixed);
        $conn = ConnectionManager::get('default');
        $sql = <<<EOT
SELECT category, kind, count(*) AS cnt, convert_id, convert_name, convert_name_jp
 FROM search_keywords
 WHERE MATCH(name_jp) AGAINST ('{$sql_keywords}' IN BOOLEAN MODE)
 OR MATCH(name) AGAINST ('{$sql_keywords}' IN BOOLEAN MODE)
 GROUP BY category, kind, convert_id, convert_name, convert_name_jp
EOT;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $search_fulltexts = $stmt->fetchAll('assoc');
//        Log::error('$sql_keywords');
//        Log::error($sql_keywords);
        Log::error('$sql');
        Log::error($sql);
//        Log::error($search_fulltexts);
        if ($search_fulltexts) {
            return $search_fulltexts;
        } else {
            return false;
        }
    }

    public function getCardData(){

        $request_data = [
            'keywords' => null,
        ];
        if ($this->controller->request->is('get')) {
            $request_data = $this->getRequestGetData();
        } else {
            $request_data = $this->getRequestPostData();
        }
        if (isset($request_data['page'])) {
            $this->controller->search_config['page'] = $request_data['page'];
        }
        Log::error('Request Data : KeywordAnalysis');
        Log::error( json_encode($request_data, JSON_UNESCAPED_UNICODE));

        $search_data = [
            'keywords' => [],
        ];
        if ($request_data['keywords']) {
            $search_data = $this->getSearchData($request_data);
        }
        Log::error('Search Data : KeywordAnalysis');
        Log::error(json_encode($search_data, JSON_UNESCAPED_UNICODE));


        if (!$search_data['keywords'] && !isset($request_data['kind']) && !isset($request_data['convert_id'])) {
            $search_mode = 'initial';
            $card_data = [];
            $total_count = 0;
        } elseif (isset($request_data['convert_id']) && !!($request_data['convert_id'])) {
            $search_mode = 'point_search';
            $card_ids = [$request_data['convert_id']];
            $result = $this->getPointData($card_ids);
            $card_data = $result['card_data'];
            $total_count = $result['total_count'];
        } elseif (isset($request_data['kind']) && !!($request_data['kind'])) {
            $search_mode = 'candidate';
            $result = $this->getCardDataByCandidate($request_data, $search_data);
            $card_data = $result['card_data'];
            $total_count = $result['total_count'];
        } else {
            $search_mode = 'fulltext-search';
            $result = $this->getCardDataBySearchbox($request_data, $search_data);
            $card_data = $result['card_data'];
            $total_count = count($result['card_ids_uniqued']);
        }

        $result_set = [
            'search_mode' => $search_mode,
            'request_data' => $request_data,
            'search_data' => $search_data,
            'result_total_count' => $total_count,
            'card_data' => $card_data,
        ];

        return $result_set;

    }

    public function getData ($card_ids)
    {
        $card_data = [];
        $cards = $this->controller->Cards->getDataByIds($card_ids)->toArray();
        if ($cards) {
            foreach ($cards as $card) {
                $card_data[$card->id] = $card;
            }
        }
        return $card_data;
    }

    public function getPointData ($card_ids)
    {
        $card_data = [];
        $cards = $this->controller->Cards->getDataByIds($card_ids)->toArray();
        Log::error($cards);
        if ($cards) {
            foreach ($cards as $card) {
                $card_data[$card->id] = $card;
            }
        }
        return [
            'card_data' => $card_data,
            'total_count' => count($cards),
        ];
    }

    public function getCardDataBySearchbox ($request_data, $search_data) {

        $this->searchwords = $search_data['keywords'];
        $this->KeywordAnalysis($this->searchwords[$this->cnt]);
//            Log::error('Search Analysed : KeywordAnalysis');
//            Log::error($this->analyzed);
        Log::error('Search Group Analysed : KeywordAnalysis');
        Log::error($this->group_analyzed);

        $card_data = [];
        $card_ids_uniqued = $this->group_analyzed;
        if ($this->group_analyzed) {
            foreach ($this->group_analyzed as $key => $value) {
                $sequence = $value['sequence'][0];
                foreach ($value['groups'] as $analysed_data) {
                    $convert_id = $analysed_data['convert_id'];
                    $data = [
                        'category' => $analysed_data['category'],
                        'kind' => $analysed_data['kind'],
                        'cnt' => $analysed_data['cnt'],
                        'id' => $analysed_data['convert_id'],
                        'name' => $analysed_data['convert_name'],
                        'name_jp' => $analysed_data['convert_name_jp'],
                        'name_kana' => 'kana',
                        'code' => 'code',
                    ];
                    $card_data[$convert_id] = $data;
                }
            }
        }
        return [
            'card_data' => $card_data,
            'card_ids_uniqued' => $card_ids_uniqued,
        ];
    }

    public function getCardDataByCandidate ($request_data, $search_data) {
        $card_data = [];
        $card_ids_uniqued = $this->group_analyzed;

        $kind = (int)$request_data['kind'];
        $sequence = $request_data['sequence'];
        switch ($kind) {
            case (52):
                $clause = [
                    'code' => $sequence,
                ];
                break;
        }

        $card_data = $this->controller->Cards->find('all')->where($clause)->limit(10)->toArray();

        return [
            'card_data' => $card_data,
            'total_count' => count($card_data),
        ];
    }

}
