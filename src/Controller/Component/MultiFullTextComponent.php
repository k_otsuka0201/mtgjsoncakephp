<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * MultiFullText component
 */
class MultiFullTextComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param array $config
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /**
     * リクエストデータの処理
     *
     * @return array
     */
    public function getRequestGetData ()
    {
        $keywords = $this->controller->request->getQuery('keywords');
        $page = $this->controller->request->getQuery('page');
        $request_data = [
            'keywords' => $keywords,
            'page' => $page,
        ];
        return $request_data;
    }

    public function getRequestPostData ()
    {
        $keywords = $this->controller->request->getData('keywords');
        $request_data = [
            'keywords' => $keywords,
            'page' => $this->controller->request->getData('page'),
        ];
        return $request_data;
    }

    /**
     * 全文検索向けの入力キーワードの処理
     *
     * @param $keywords
     * @return array|false|string[]
     */
    public function getSearchData ($request_data)
    {
        $search_data = [];
        $keywords = $request_data['keywords'];

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data['keywords'] = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        return $search_data;
    }

    public function getCardData(){

        if ($this->controller->request->is('get')) {
            $request_data = $this->getRequestGetData();
        } else {
            $request_data = $this->getRequestPostData();
        }
        if (isset($request_data['page'])) {
            $this->controller->search_config['page'] = $request_data['page'];
        }
        Log::error('Request Data: ' . json_encode($request_data, JSON_UNESCAPED_UNICODE));

        $search_data = [
            'keywords' => [],
        ];
        if ($request_data['keywords']) {
            $search_data = $this->getSearchData($request_data);
        }
        Log::error('Search Data: ' . json_encode($search_data, JSON_UNESCAPED_UNICODE));


        if (!$search_data['keywords']) {

            $search_fulltexts = [];
            $search_fulltexts_sliced = [];
            $card_data = [];

        } else {

            $keywords_prefixed = [];
            foreach ($search_data['keywords'] as $keyword) {
                $keywords_prefixed[] = '+' . $keyword;
            }
            $sql_keywords = implode(' ', $keywords_prefixed);

            $conn = ConnectionManager::get('default');
            $stmt = $conn->prepare(
                "SELECT * FROM search_multi_fulltext WHERE MATCH(name_jp) AGAINST ('{$sql_keywords}' IN BOOLEAN MODE) OR MATCH(name) AGAINST ('{$sql_keywords}' IN BOOLEAN MODE)"
            );
            $stmt->execute();
            $search_fulltexts = $stmt->fetchAll('assoc');

            $search_fulltexts_sliced = [];
            $card_data = [];
            if ($search_fulltexts) {
                $offset = $this->controller->search_config['page'] - 1;
                $per_page = $this->controller->search_config['per_page'];
                $search_fulltexts_sliced = array_slice($search_fulltexts, $offset, $per_page, false);

                $card_ids = [];
                foreach ($search_fulltexts_sliced as $search_fulltext) {
                    $card_ids[] = $search_fulltext['card_id'];
                }
                $cards = $this->controller->Cards->getDataByIds($card_ids)->toArray();
                if ($cards) {
                    foreach ($cards as $card) {
                        $card_data[$card->id] = $card;
                    }
                }
            }

        }

        $result_set = [
            'request_data' => $request_data,
            'search_data' => $search_data,
            'result_total_count' => count($search_fulltexts),
            'results' => $search_fulltexts_sliced,
            'card_data' => $card_data,
        ];

        return $result_set;

    }

}
