<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * Search component
 */
class SearchComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param array $config
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /**
     * リクエストデータの処理
     *
     * @return array
     */
    public function getRequestData ()
    {
        $request_data = [
            'keywords' => $this->getController()->request->getData('keywords'),
            'page' => $this->getController()->request->getData('page'),
        ];
        return $request_data;
    }

    /**
     * 全文検索向けの入力キーワードの処理
     *
     * @param $keywords
     * @return array|false|string[]
     */
    public function getFulltextSearchData ($request_data)
    {
        $search_data = [];
        $keywords = $request_data['keywords'];

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data['keywords'] = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        return $search_data;
    }

    public function getSearchAnalyzed ($request_data)
    {
        $search_data = [];
        $keywords = $request_data['keywords'];

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data['keywords'] = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        //
        $sql_keyword = $search_data['keywords'][0];

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare(
            "SELECT * FROM search_keywords WHERE MATCH(name) AGAINST ('{$sql_keyword}' IN BOOLEAN MODE)"
        );
        $stmt->execute();
        $search_keywords = $stmt->fetchAll('assoc');

        Log::error($search_keywords);


        return $search_keywords;
    }

    /**
     * 入力キーワードの処理
     *
     * @param $keywords
     * @return array|false|string[]
     */
    public function getSearchData ($request_data)
    {
        $search_data = [];
        $keywords = $request_data['keywords'];

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data['keywords'] = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        // 特定のワードが属性変換テーブルに一致した場合、キーワードを変換する
        $convert_attributes = $this->controller->ConvertAttributes->getDataByNames($search_data['keywords'])
            ->combine('name', 'name_converted')
            ->toArray();
        foreach ($search_data['keywords'] as $key => $keyword) {
            if (array_key_exists($keyword, $convert_attributes)) {
                $search_data['keywords'][$key] = $convert_attributes[$keyword];
            }
        }

        return $search_data;
    }

}
