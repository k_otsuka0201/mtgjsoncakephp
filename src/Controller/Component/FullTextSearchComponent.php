<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * FullTextSearch component
 */
class FullTextSearchComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param array $config
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }

    /**
     * Getパラメータを受け取る
     *
     * @return array
     */
    public function getRequestGetData ()
    {
        $keywords = $this->controller->request->getQuery('keywords');
        $request_data = [
            'searchmode' => $this->controller->request->getQuery('searchmode'),
            'keywords' => ($keywords) ? $keywords : null ,
            'page' => $this->controller->request->getQuery('page'),
            'sequence' => $this->controller->request->getQuery('sequence'),
            'category' => $this->controller->request->getQuery('category'),
            'kind' => $this->controller->request->getQuery('kind'),
            'convert_id' => $this->controller->request->getQuery('convert_id'),
        ];
        return $request_data;
    }

    /**
     * Postパラメータを受け取る
     *
     * @return array
     */
    public function getRequestPostData ()
    {
        $keywords = $this->controller->request->getData('keywords');
        $request_data = [
            'searchmode' => $this->controller->request->getData('searchmode'),
            'keywords' => ($keywords) ? $keywords : null ,
            'page' => $this->controller->request->getData('page'),
            'sequence' => $this->controller->request->getData('sequence'),
            'category' => $this->controller->request->getData('category'),
            'kind' => $this->controller->request->getData('kind'),
            'convert_id' => $this->controller->request->getData('convert_id'),
        ];
        return $request_data;
    }

    /**
     * 全文検索向けの入力キーワードの処理
     *
     * @param $keywords
     * @return array|false|string[]
     */
    public function getSearchData ($request_data)
    {
        $search_data = [];
        $keywords = $request_data['keywords'];
        if (!$keywords) {
            return [
                'keywords' => [],
            ];
        }

        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data['keywords'] = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        return $search_data;
    }
}
