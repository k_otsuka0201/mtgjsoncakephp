<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cardsets Controller
 *
 * @property \App\Model\Table\CardsetsTable $Cardsets
 *
 * @method \App\Model\Entity\Cardset[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CardsetsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $cardsets = $this->paginate($this->Cardsets);

        $this->set(compact('cardsets'));
    }

    /**
     * View method
     *
     * @param string|null $id Cardset id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cardset = $this->Cardsets->get($id, [
            'contain' => ['CardsBigweb']
        ]);

        $this->set('cardset', $cardset);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cardset = $this->Cardsets->newEntity();
        if ($this->request->is('post')) {
            $cardset = $this->Cardsets->patchEntity($cardset, $this->request->getData());
            if ($this->Cardsets->save($cardset)) {
                $this->Flash->success(__('The cardset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cardset could not be saved. Please, try again.'));
        }
        $this->set(compact('cardset'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cardset id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cardset = $this->Cardsets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cardset = $this->Cardsets->patchEntity($cardset, $this->request->getData());
            if ($this->Cardsets->save($cardset)) {
                $this->Flash->success(__('The cardset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cardset could not be saved. Please, try again.'));
        }
        $this->set(compact('cardset'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cardset id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cardset = $this->Cardsets->get($id);
        if ($this->Cardsets->delete($cardset)) {
            $this->Flash->success(__('The cardset has been deleted.'));
        } else {
            $this->Flash->error(__('The cardset could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
