<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Log\Log;

/**
 * MultiFullText Controller
 *
 *
 * @method \App\Model\Entity\MultiFullText[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MultiFullTextController extends AppController
{

    /**
     * SearchesController constructor.
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Cards');
        $this->loadComponent('MultiFullText');

        $this->search_config = [
            'page' => 1,
            'per_page' => 10,
        ];
    }

    public function search()
    {

        $this->autoRender = false;
        $this->viewBuilder()->setLayout(false);

        $this->request->accepts('application/json');

        $result_sets = $this->MultiFullText->getCardData();

        $result_total_count = $result_sets['result_total_count'];
        $result_total_page = ceil($result_sets['result_total_count']/$this->search_config['per_page']);
        $page_prev = (($this->search_config['page'] - 1) > 0) ? $this->search_config['page'] - 1 : null ;
        $page_next = ($this->search_config['page'] + 1 > $result_total_page) ? null : $this->search_config['page'] + 1 ;
        $record_from = (($this->search_config['page'] - 1) * $this->search_config['per_page']) + 1 ;
        $record_to = (($this->search_config['page']) * $this->search_config['per_page']) ;

        $result = [
            'result_total_count' => $result_total_count,
            'result_total_page' => $result_total_page,
            'page' => $this->search_config['page'],
            'page_prev' => $page_prev,
            'page_next' => $page_next,
            'record_from' => $record_from,
            'record_to' => $record_to,
            'results' => $result_sets['results'],
            'card_data' => $result_sets['card_data'],
            'searching' => [
                'request_data' => $result_sets['request_data'],
                'search_data' => $result_sets['search_data'],
            ],
        ];

        $this->response->getCharset('UTF-8');
        $this->response->getType('json');
        $this->response->header('Access-Control-Allow-Origin: *');
        $this->response->header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

        return $this->response->withStringBody(json_encode($result, JSON_UNESCAPED_UNICODE));

    }
}
