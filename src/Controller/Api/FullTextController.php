<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Log\Log;

/**
 * MultiFullText Controller
 *
 *
 * @method \App\Model\Entity\SearchFullText[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FullTextController extends AppController
{
    public $kind_array = [
        0 => 'Category Card',
        1 => '英語名',
        2 => '日本語名',
        3 => '日本語名かな',
        4 => 'Facename',

        11 => 'Type',
        12 => 'タイプ',

        21 => 'Rarity',

        31 => 'Color',

        41 => 'Color Identity',
        42 => 'Colors',
        43 => 'Types',
        44 => 'Subtypes',
        45 => 'Supertypes',
        46 => 'Keywords',

        51 => 'Layout',
        52 => 'Code',
        53 => 'Border Color',
        54 => 'Frame Version',

        61 => 'Format',

        71 => 'Flavor Text',
        72 => 'フレーバーテキスト',
        73 => 'Text',
        74 => 'テキスト',

        101 => 'Category Cardset',
        101 => 'Cardset Name',
        102 => 'カードセット名',
        103 => 'Cardset Code',
        104 => 'カードセット名かな',

        200 => 'Category Rarity',
        201 => 'Rarity Name',
        202 => 'レアリティ名',
        203 => 'Rarity Code',

        300 => 'Category Color',
        301 => 'Color Name',
        302 => 'カラー名',
        303 => 'Color Code',
    ];

    /**
     * SearchesController constructor.
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Cards');
        $this->loadComponent('Card');
        $this->loadComponent('FullTextSearch');
        $this->loadComponent('FullTextAnalysis');

        $this->search_config = [
            'page' => 1,
            'per_page' => 10,
        ];
    }

    public function search()
    {

        $this->autoRender = false;
        $this->viewBuilder()->setLayout(false);

        $this->request->accepts('application/json');

        $result_sets = $this->execute();

        $result_total_count = $result_sets['result_total_count'];
        $result_total_page = ceil($result_sets['result_total_count']/$this->search_config['per_page']);
        $page_prev = (($this->search_config['page'] - 1) > 0) ? $this->search_config['page'] - 1 : null ;
        $page_next = ($this->search_config['page'] + 1 > $result_total_page) ? null : $this->search_config['page'] + 1 ;
        $record_from = (($this->search_config['page'] - 1) * $this->search_config['per_page']) + 1 ;
        $record_to = (($this->search_config['page']) * $this->search_config['per_page']) ;

        $result = [
            'result_total_count' => $result_total_count,
            'result_total_page' => $result_total_page,
            'page' => $this->search_config['page'],
            'page_prev' => $page_prev,
            'page_next' => $page_next,
            'record_from' => $record_from,
            'record_to' => $record_to,
            'analyzed_data' => $result_sets['analyzed_data'],
            'point_data' => $result_sets['card_data'],
            'kinds' => $this->kind_array,
            'searching' => [
                'request_data' => $result_sets['request_data'],
                'search_data' => $result_sets['search_data'],
            ],
        ];

        $this->response->getCharset('UTF-8');
        $this->response->getType('json');
        $this->response->header('Access-Control-Allow-Origin: *');
        $this->response->header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

        return $this->response->withStringBody(json_encode($result, JSON_UNESCAPED_UNICODE));

    }

    protected function execute ()
    {
        $request_data = $this->FullTextSearch->getRequestPostData();
        Log::error('Request Data: FullText');
        Log::error( json_encode($request_data, JSON_UNESCAPED_UNICODE));

        $search_data = $this->FullTextSearch->getSearchData($request_data);
        Log::error('Search Data: FullText');
        Log::error(json_encode($search_data, JSON_UNESCAPED_UNICODE));

        $group_analyzed = $this->FullTextAnalysis->KeywordAnalysisExecute($search_data);
        Log::error('Search Group Analysed: FullText');
        Log::error($group_analyzed);

        $cards = $this->Card->getCardsByAnalyzed($group_analyzed);
//        Log::error('Search Group Analysed: Card');
//        Log::error($cards);
        $cards_paginated = $this->paginate($cards, [
            'limit' => $this->search_config['per_page'],
        ]);
        $total_count = $cards->count();

        $result_set = [
            'request_data' => $request_data,
            'search_data' => $search_data,
            'result_total_count' => $total_count,
            'analyzed_data' => $group_analyzed,
            'card_data' => $cards_paginated,
        ];

        return $result_set;
    }
}
