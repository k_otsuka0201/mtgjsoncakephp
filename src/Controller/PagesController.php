<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Log\Log;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));



        $this->loadModel('Attributes');
        $this->loadModel('Cards');
        $this->loadModel('Cardsets');
        $this->loadModel('Searches');
        $request_data = null;
        $search_data = null;
        $result = null;
        if ($this->request->is('post')) {
            $request_data = $this->getRequestData();
            $search_data = $this->getSearchData($request_data['keywords']);
            $result = $this->executeSearch($search_data);
        }
        $this->set([
            'request_data' => $request_data,
            'search_data' => $search_data,
            'result' => $result,
        ]);



        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function getRequestData ()
    {
        $result = [
            'keywords' => $this->request->getData('keywords'),
        ];

        Log::error('Request Data: ' . json_encode($result, JSON_UNESCAPED_UNICODE));
        return $result;
    }

    public function getSearchData ($keywords)
    {
        // 全角英数字記号を半角に変換、全半角カタカナを全角ひらがなに変換
        $keywords_converted = str_replace(['”', '’', '￥', '〜'], ['"', '\'', '¥', '~'], mb_convert_kana($keywords, "HVac"));

        // トリミングをして、'sを省いたものから、全角の漢字平仮名、半角英数字以外を削除
        $keywords_removed = preg_replace('/[^\sぁ-んーa-zA-Z0-9一-龠\-\r]+/u','' , str_replace('\'s', '', $keywords_converted));

        // 半角スペース区切りで配列にして、空き文字列は配列にいれない
        $search_data = preg_split('/[\s]+/', $keywords_removed, -1, PREG_SPLIT_NO_EMPTY);

        Log::error('Search Data: ' . json_encode($search_data, JSON_UNESCAPED_UNICODE));
        return $search_data;
    }

    public function executeSearch ($search_data)
    {

        $search_data_analyzed = [];
        $result_card_ids = [];
        $result_ids = [];
        $results = [];

        /**
         * 各検索ワードに一致する属性データ
         */
        $attributes = $this->Attributes->find('all')
            ->where([
                'name IN' => $search_data
            ])->toArray();
        $results['attributes'] = $attributes;

        foreach ($attributes as $attribute) {
            $search_data_analyzed[$attribute['name']]['type'][] = $attribute['type'];
        }

        /**
         * カード名に完全一致するカードデータ
         */
        $cardname_ids = $this->Searches->find('all')
            ->where([
                'Searches.name' => implode(' ', $search_data),
            ])
            ->extract('card_id')
            ->toArray();
        $result_ids['cardnames'] = $cardname_ids;

        /**
         * 各検索ワードを全て含むカードデータ
         */
        $hits = [];
        $hits[] = [];
        foreach ($search_data as $tmp) {
            $hits[] = $this->Searches->find('all')
                ->where([
                    'Searches.name IN' => [$tmp]
                ])
                ->extract('card_id')
                ->toArray();
        }
        $result_ids['cards'] = call_user_func_array('array_intersect', $hits);

        /**
         *
         */
        $hits = [];
        foreach ($search_data as $tmp) {
            $hits[] = $this->Searches
                ->find('all')
                ->select(['name', 'card_id', 'type'])
                ->where([
                    'Searches.name IN' => [$tmp]
                ])
                ->groupBy('type')
                ->toArray();
        }

        foreach ($hits as $value_sets) {
            foreach ($value_sets as $key => $values) {
                foreach ($values as $value) {
                    $result_ids[$key . '-' . $value->name][] = $value->card_id;
                }
            }
        }

        Log::error($result_ids);

        /**
         *
         */
        $card_ids_merged = call_user_func_array('array_merge', $result_ids);
        $card_ids_uniqued = array_unique($card_ids_merged);

        $result_data = $this->Cards->find('all')
            ->where([
                'id IN' => $card_ids_uniqued,
            ])->toArray();

        $result_converted = [];
        foreach ($result_data as $datum) {
            $result_converted[$datum->id] = $datum;
        }

        foreach ($result_ids as $key => $ids) {
            foreach ($ids as $id) {
                $results[$key][$id] = $result_converted[$id];
            }
        }

        return $results;

    }

}
