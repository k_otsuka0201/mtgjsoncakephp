<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SearchCards Controller
 *
 * @property \App\Model\Table\SearchCardsTable $SearchCards
 *
 * @method \App\Model\Entity\SearchCard[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SearchCardsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cards']
        ];
        $searchCards = $this->paginate($this->SearchCards);

        $this->set(compact('searchCards'));
    }

    /**
     * View method
     *
     * @param string|null $id Search Card id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $searchCard = $this->SearchCards->get($id, [
            'contain' => ['Cards']
        ]);

        $this->set('searchCard', $searchCard);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $searchCard = $this->SearchCards->newEntity();
        if ($this->request->is('post')) {
            $searchCard = $this->SearchCards->patchEntity($searchCard, $this->request->getData());
            if ($this->SearchCards->save($searchCard)) {
                $this->Flash->success(__('The search card has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The search card could not be saved. Please, try again.'));
        }
        $cards = $this->SearchCards->Cards->find('list', ['limit' => 200]);
        $this->set(compact('searchCard', 'cards'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Search Card id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $searchCard = $this->SearchCards->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $searchCard = $this->SearchCards->patchEntity($searchCard, $this->request->getData());
            if ($this->SearchCards->save($searchCard)) {
                $this->Flash->success(__('The search card has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The search card could not be saved. Please, try again.'));
        }
        $cards = $this->SearchCards->Cards->find('list', ['limit' => 200]);
        $this->set(compact('searchCard', 'cards'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Search Card id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $searchCard = $this->SearchCards->get($id);
        if ($this->SearchCards->delete($searchCard)) {
            $this->Flash->success(__('The search card has been deleted.'));
        } else {
            $this->Flash->error(__('The search card could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
