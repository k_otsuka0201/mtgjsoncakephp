<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConvertCardsets Controller
 *
 * @property \App\Model\Table\ConvertCardsetsTable $ConvertCardsets
 *
 * @method \App\Model\Entity\ConvertCardset[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConvertCardsetsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $convertCardsets = $this->paginate($this->ConvertCardsets);

        $this->set(compact('convertCardsets'));
    }

    /**
     * View method
     *
     * @param string|null $id Convert Cardset id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convertCardset = $this->ConvertCardsets->get($id, [
            'contain' => []
        ]);

        $this->set('convertCardset', $convertCardset);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convertCardset = $this->ConvertCardsets->newEntity();
        if ($this->request->is('post')) {
            $convertCardset = $this->ConvertCardsets->patchEntity($convertCardset, $this->request->getData());
            if ($this->ConvertCardsets->save($convertCardset)) {
                $this->Flash->success(__('The convert cardset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The convert cardset could not be saved. Please, try again.'));
        }
        $this->set(compact('convertCardset'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Convert Cardset id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convertCardset = $this->ConvertCardsets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convertCardset = $this->ConvertCardsets->patchEntity($convertCardset, $this->request->getData());
            if ($this->ConvertCardsets->save($convertCardset)) {
                $this->Flash->success(__('The convert cardset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The convert cardset could not be saved. Please, try again.'));
        }
        $this->set(compact('convertCardset'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Convert Cardset id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $convertCardset = $this->ConvertCardsets->get($id);
        if ($this->ConvertCardsets->delete($convertCardset)) {
            $this->Flash->success(__('The convert cardset has been deleted.'));
        } else {
            $this->Flash->error(__('The convert cardset could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
