<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConvertAttributes Controller
 *
 * @property \App\Model\Table\ConvertAttributesTable $ConvertAttributes
 *
 * @method \App\Model\Entity\ConvertAttribute[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConvertAttributesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $convertAttributes = $this->paginate($this->ConvertAttributes);

        $this->set(compact('convertAttributes'));
    }

    /**
     * View method
     *
     * @param string|null $id Convert Attribute id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convertAttribute = $this->ConvertAttributes->get($id, [
            'contain' => []
        ]);

        $this->set('convertAttribute', $convertAttribute);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convertAttribute = $this->ConvertAttributes->newEntity();
        if ($this->request->is('post')) {
            $convertAttribute = $this->ConvertAttributes->patchEntity($convertAttribute, $this->request->getData());
            if ($this->ConvertAttributes->save($convertAttribute)) {
                $this->Flash->success(__('The convert attribute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The convert attribute could not be saved. Please, try again.'));
        }
        $this->set(compact('convertAttribute'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Convert Attribute id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convertAttribute = $this->ConvertAttributes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convertAttribute = $this->ConvertAttributes->patchEntity($convertAttribute, $this->request->getData());
            if ($this->ConvertAttributes->save($convertAttribute)) {
                $this->Flash->success(__('The convert attribute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The convert attribute could not be saved. Please, try again.'));
        }
        $this->set(compact('convertAttribute'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Convert Attribute id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $convertAttribute = $this->ConvertAttributes->get($id);
        if ($this->ConvertAttributes->delete($convertAttribute)) {
            $this->Flash->success(__('The convert attribute has been deleted.'));
        } else {
            $this->Flash->error(__('The convert attribute could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
