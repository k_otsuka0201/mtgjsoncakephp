<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CardsetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CardsetsTable Test Case
 */
class CardsetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CardsetsTable
     */
    public $Cardsets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cardsets',
        'app.cards_bigweb'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Cardsets') ? [] : ['className' => CardsetsTable::class];
        $this->Cardsets = TableRegistry::getTableLocator()->get('Cardsets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cardsets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
