<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConvertAttributesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConvertAttributesTable Test Case
 */
class ConvertAttributesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConvertAttributesTable
     */
    public $ConvertAttributes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convert_attributes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ConvertAttributes') ? [] : ['className' => ConvertAttributesTable::class];
        $this->ConvertAttributes = TableRegistry::getTableLocator()->get('ConvertAttributes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConvertAttributes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
