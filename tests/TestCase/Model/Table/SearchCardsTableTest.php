<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SearchCardsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SearchCardsTable Test Case
 */
class SearchCardsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SearchCardsTable
     */
    public $SearchCards;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.search_cards',
        'app.cards'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SearchCards') ? [] : ['className' => SearchCardsTable::class];
        $this->SearchCards = TableRegistry::getTableLocator()->get('SearchCards', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SearchCards);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
