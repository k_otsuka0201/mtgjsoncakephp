<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SearchKeywordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SearchKeywordsTable Test Case
 */
class SearchKeywordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SearchKeywordsTable
     */
    public $SearchKeywords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.search_keywords'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SearchKeywords') ? [] : ['className' => SearchKeywordsTable::class];
        $this->SearchKeywords = TableRegistry::getTableLocator()->get('SearchKeywords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SearchKeywords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
