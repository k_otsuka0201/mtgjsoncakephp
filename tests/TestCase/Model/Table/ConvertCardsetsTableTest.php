<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConvertCardsetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConvertCardsetsTable Test Case
 */
class ConvertCardsetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConvertCardsetsTable
     */
    public $ConvertCardsets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convert_cardsets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ConvertCardsets') ? [] : ['className' => ConvertCardsetsTable::class];
        $this->ConvertCardsets = TableRegistry::getTableLocator()->get('ConvertCardsets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConvertCardsets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
