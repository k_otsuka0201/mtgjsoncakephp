<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SearchMultiFulltextTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SearchMultiFulltextTable Test Case
 */
class SearchMultiFulltextTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SearchMultiFulltextTable
     */
    public $SearchMultiFulltext;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.search_multi_fulltext',
        'app.cards'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SearchMultiFulltext') ? [] : ['className' => SearchMultiFulltextTable::class];
        $this->SearchMultiFulltext = TableRegistry::getTableLocator()->get('SearchMultiFulltext', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SearchMultiFulltext);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
