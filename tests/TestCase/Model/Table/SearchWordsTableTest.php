<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SearchWordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SearchWordsTable Test Case
 */
class SearchWordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SearchWordsTable
     */
    public $SearchWords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.search_words',
        'app.cardsets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SearchWords') ? [] : ['className' => SearchWordsTable::class];
        $this->SearchWords = TableRegistry::getTableLocator()->get('SearchWords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SearchWords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
