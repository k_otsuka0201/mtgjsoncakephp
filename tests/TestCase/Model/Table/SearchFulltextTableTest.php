<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SearchFulltextTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SearchFulltextTable Test Case
 */
class SearchFulltextTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SearchFulltextTable
     */
    public $SearchFulltext;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.search_fulltext',
        'app.cards'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SearchFulltext') ? [] : ['className' => SearchFulltextTable::class];
        $this->SearchFulltext = TableRegistry::getTableLocator()->get('SearchFulltext', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SearchFulltext);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
