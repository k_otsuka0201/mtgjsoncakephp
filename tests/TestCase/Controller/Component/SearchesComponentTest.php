<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\SearchesComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\SearchesComponent Test Case
 */
class SearchesComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\SearchesComponent
     */
    public $Searches;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Searches = new SearchesComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Searches);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
