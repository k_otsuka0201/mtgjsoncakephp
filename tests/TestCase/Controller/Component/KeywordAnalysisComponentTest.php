<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\KeywordAnalysisComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\KeywordAnalysisComponent Test Case
 */
class KeywordAnalysisComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\KeywordAnalysisComponent
     */
    public $KeywordAnalysis;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->KeywordAnalysis = new KeywordAnalysisComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KeywordAnalysis);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
