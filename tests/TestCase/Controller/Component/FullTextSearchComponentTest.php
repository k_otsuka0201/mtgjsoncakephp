<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\FullTextSearchComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\FullTextSearchComponent Test Case
 */
class FullTextSearchComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\FullTextSearchComponent
     */
    public $FullTextSearch;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->FullTextSearch = new FullTextSearchComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FullTextSearch);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
