<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\FullTextAnalysisComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\FullTextAnalysisComponent Test Case
 */
class FullTextAnalysisComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\FullTextAnalysisComponent
     */
    public $FullTextAnalysis;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->FullTextAnalysis = new FullTextAnalysisComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FullTextAnalysis);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
