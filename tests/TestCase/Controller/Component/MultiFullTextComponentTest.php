<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\MultiFullTextComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\MultiFullTextComponent Test Case
 */
class MultiFullTextComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\MultiFullTextComponent
     */
    public $MultiFullText;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->MultiFullText = new MultiFullTextComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MultiFullText);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
