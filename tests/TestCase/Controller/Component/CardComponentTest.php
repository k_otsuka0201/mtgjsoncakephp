<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\CardComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\CardComponent Test Case
 */
class CardComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\CardComponent
     */
    public $Card;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Card = new CardComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Card);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
