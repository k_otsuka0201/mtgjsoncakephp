<?php
namespace App\Test\TestCase\Shell;

use App\Shell\UpdateKanaOfDuplexShell;
use Cake\TestSuite\ConsoleIntegrationTestCase;

/**
 * App\Shell\UpdateKanaOfDuplexShell Test Case
 */
class UpdateKanaOfDuplexShellTest extends ConsoleIntegrationTestCase
{

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject
     */
    public $io;

    /**
     * Test subject
     *
     * @var \App\Shell\UpdateKanaOfDuplexShell
     */
    public $UpdateKanaOfDuplex;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')->getMock();
        $this->UpdateKanaOfDuplex = new UpdateKanaOfDuplexShell($this->io);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UpdateKanaOfDuplex);

        parent::tearDown();
    }

    /**
     * Test getOptionParser method
     *
     * @return void
     */
    public function testGetOptionParser()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
