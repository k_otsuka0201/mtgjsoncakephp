<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

$DBSERVER = 'localhost';
$DBUSER = 'root';
$DBPASS = 'Root+4124';

$mysqli = new mysqli($DBSERVER,$DBUSER,$DBPASS, 'mtgjson');

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n<br>", mysqli_connect_error());
    exit();
} else {
    print("Connect success<br>");
}

if (!$mysqli->set_charset("utf8mb4")) {
    printf("Error loading character set utf8mb4: %s\n", $mysqli->error);
    exit();
} else {
    printf("Current character set: %s\n<br>", $mysqli->character_set_name());
}

$query = "INSERT INTO cardsets (`name`, `name_jp`, `type`, `block`, `code`, `format`, `baseSetSize`, `totalSetSize`, `isFoilOnly`, `release_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
$stmt = $mysqli->prepare($query);

require '../vendor/autoload.php'; // include Composer's autoloader

echo '--- client ---<br>';
$client = new MongoDB\Client("mongodb://localhost:27017");
//var_dump($client);

echo '--- cluster ---<br>';
$cluster = $client->mtgjson;
//var_dump($cluster);

echo '--- collection ---<br>';
$collection = $cluster->SetList;
//var_dump($collection);

$result = $collection->find()->toArray();
$cardsets = (array)$result[0]['data'];
foreach ($cardsets as $cardset) {
	$name = $cardset['name'];
	$name_jp = (isset($cardset['translations']['Japanese'])) ? str_replace(['『', '』'], '', $cardset['translations']['Japanese']) : null ;
    $type = $cardset['type'];
    $block = (isset($cardset['block'])) ? $cardset['block'] : null ;
    $code = $cardset['code'];
    $format = null;
    $baseSetSize = $cardset['baseSetSize'];
    $totalSetSize = $cardset['totalSetSize'];
    $isFoilOnly = $cardset['isFoilOnly'];
    $release_date = $cardset['releaseDate'];

	$stmt->bind_param("ssssssddds", $name, $name_jp, $type, $block, $code, $format, $baseSetSize, $totalSetSize, $isFoilOnly, $release_date);
	$stmt->execute();
}

$stmt->close();

$mysqli->close();

