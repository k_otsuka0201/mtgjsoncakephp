<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

$DBSERVER = 'localhost';
$DBUSER = 'root';
$DBPASS = 'Root+4124';

$mysqli = new mysqli($DBSERVER,$DBUSER,$DBPASS, 'mtgjson');

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n<br>", mysqli_connect_error());
    exit();
} else {
    print("Connect success<br>");
}

if (!$mysqli->set_charset("utf8mb4")) {
    printf("Error loading character set utf8mb4: %s\n", $mysqli->error);
    exit();
} else {
    printf("Current character set: %s\n<br>", $mysqli->character_set_name());
}

/**
INSERT INTO `attributes` (`id`, `name`, `name_jp`, `name_kana`, `type`, `code`) VALUES
(52, 'common', 'コモン', 'こもん', 'rarity', NULL),
(53, 'mythic', '神話レア', 'しんわれあ', 'rarity', NULL),
(54, 'rare', 'レア', 'れあ', 'rarity', NULL),
(55, 'uncommon', 'アンコモン', 'あんこもん', 'rarity', NULL),
(905, 'Black', '黒', 'くろ', 'color', 'B'),
(906, 'Green', '緑', 'みどり', 'color', 'G'),
(907, 'Red', '赤', 'あか', 'color', 'R'),
(908, 'Blue', '青', 'あお', 'color', 'U'),
(909, 'White', '白', 'しろ', 'color', 'W'),
(910, 'Standard', 'スタンダード', 'すたんだーど', 'format', NULL);
 */

$query = "INSERT INTO attributes (`name`, `type`) VALUES (?, ?)";
$stmt = $mysqli->prepare($query);

require '../vendor/autoload.php'; // include Composer's autoloader

echo '--- client ---<br>';
$client = new MongoDB\Client("mongodb://localhost:27017");
//var_dump($client);

echo '--- cluster ---<br>';
$cluster = $client->mtgjson;
//var_dump($cluster);

echo '--- collection ---<br>';
$collection = $cluster->EnumValues;
//var_dump($collection);

$result = $collection->find()->toArray();
$data = (array)$result[0]['data'];
$attribute_data = [];
foreach ($data as $key => $datum) {
    if (in_array($key, ['card', 'keywords'])) {
        foreach ((array)$datum as $key_datum => $value) {
            if (in_array($key_datum, ['borderColor', 'colors', 'frameEffects', 'frameVersion', 'layout', 'rarity', 'subtypes', 'supertypes', 'types', 'abilityWords', 'keywordAbilities', 'keywordActions'])) {
                foreach ($value as $v) {
                    $d = [];
                    $d['type'] = $key_datum;
                    $d['name'] = $v;
                    $attribute_data[] = $d;
                }
            }
        }
    }
}
foreach ($attribute_data as $attribute) {
	$stmt->bind_param("ss", $attribute['name'], $attribute['type']);
	$stmt->execute();
}

$stmt->close();

$mysqli->close();

