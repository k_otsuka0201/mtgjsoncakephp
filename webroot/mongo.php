<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

$DBSERVER = 'localhost';
$DBUSER = 'root';
$DBPASS = 'Root+4124';

$mysqli = new mysqli($DBSERVER,$DBUSER,$DBPASS, 'mtgjson');

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n<br>", mysqli_connect_error());
    exit();
} else {
    print("Connect success<br>");
}

if (!$mysqli->set_charset("utf8mb4")) {
    printf("Error loading character set utf8mb4: %s\n", $mysqli->error);
    exit();
} else {
    printf("Current character set: %s\n<br>", $mysqli->character_set_name());
}

/*
INSERT INTO cards_kana (name, name_jp, name_kana)
 SELECT DISTINCT(name), name_jp, name_kana FROM `cards_bigweb` WHERE `name_kana` IS NOT NULL AND `name_kana` != ''

UPDATE `cards`
 INNER JOIN cards_kana ON cards.name = cards_kana.name
 SET cards.name_kana = cards_kana.name_kana

UPDATE `cards`
 INNER JOIN cards_kana ON cards.name = cards_kana.name
 SET cards.name_jp = cards_kana.name_jp
 WHERE cards.name_jp IS NULL

colorが設定できない: Garruk, Cursed Huntsman Emblem
colorが設定できない: On an Adventure
colorが設定できない: Adaptive Shimmerer
colorが設定できない: Farfinder
colorが設定できない: Mysterious Egg
colorが設定できない: Mysterious Egg
colorが設定できない: Narset of the Ancient Way Emblem
colorが設定できない: Companion
colorが設定できない: Ability Punchcard
colorが設定できない: Ability Punchcard
colorが設定できない: Ugin, the Spirit Dragon
colorが設定できない: Ugin, the Spirit Dragon
colorが設定できない: Ugin, the Spirit Dragon
colorが設定できない: Basri Ket Emblem
colorが設定できない: Garruk, Unleashed Emblem
colorが設定できない: Liliana, Waker of the Dead Emblem
colorが設定できない: Copy

 */

$query = "INSERT INTO cards (name, name_jp, rarity, color, colorIdentity, colors, flavorText, flavorText_jp, text, text_jp, type, type_jp, types, layout, subtypes, supertypes, code, borderColor, frameVersion, keywords, faceName, side) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
$stmt = $mysqli->prepare($query);

require '../vendor/autoload.php'; // include Composer's autoloader

echo '--- client ---<br>';
$client = new MongoDB\Client("mongodb://localhost:27017");
//var_dump($client);

echo '--- cluster ---<br>';
$cluster = $client->mtgjson;
//var_dump($cluster);

echo '--- collection ---<br>';
$collection = $cluster->Standard;
//var_dump($collection);

$result = $collection->find()->toArray();
$cardsets = $result[0]['data'];
foreach ($cardsets as $cardset_name => $cardset) {
    echo $cardset_name;

    foreach ($cardset['cards'] as $card) {
        $name = $card['name'];
        $name_jp = (isset($card['foreignData'][4]['name'])) ? $card['foreignData'][4]['name'] : null ;
        $name_kana = null;
        $rarity = $card['rarity'];
        $color = null;
        $colorIdentity = (isset($card['colorIdentity']) && count($card['colorIdentity'])) ? implode(',', get_object_vars($card['colorIdentity'])) : null ;
        $colors = (isset($card['colors']) && count($card['colors'])) ? implode(',', get_object_vars($card['colors'])) : null ;
        $flavorText = (isset($card['flavorText'])) ? $card['flavorText'] : null ;
        $flavorText_jp = (isset($card['foreignData'][4]['flavorText'])) ? $card['foreignData'][4]['flavorText'] : null ;
        $text = (isset($card['text'])) ? $card['text'] : null ;
        $text_jp = (isset($card['foreignData'][4]['text'])) ? $card['foreignData'][4]['text'] : null ;
        $type = $card['type'];
        $type_jp = (isset($card['foreignData'][4]['type'])) ? $card['foreignData'][4]['type'] : null ;
        $types = (isset($card['types']) && count($card['types'])) ? implode(',', get_object_vars($card['types'])) : null ;
        $layout = (isset($card['layout'])) ? $card['layout'] : null ;
        $subtypes = (isset($card['subtypes']) && count($card['subtypes'])) ? implode(',', get_object_vars($card['subtypes'])) : null ;
        $supertypes = (isset($card['supertypes']) && count($card['supertypes'])) ? implode(',', get_object_vars($card['supertypes'])) : null ;
        $code = (isset($card['setCode'])) ? $card['setCode'] : null ;
        $borderColor = (isset($card['borderColor'])) ? $card['borderColor'] : null ;
        $frameVersion = (isset($card['frameVersion'])) ? $card['frameVersion'] : null ;
        $keywords = (isset($card['keywords']) && count($card['keywords'])) ? implode(',', get_object_vars($card['keywords'])) : null ;
        $faceName = (isset($card['faceName'])) ? $card['faceName'] : null ;
        $side = (isset($card['side'])) ? $card['side'] : null ;

        if (!$colors && strpos($type, 'Land') !== false) {
            $color = 'Colorless';
        } elseif (!$colors && strpos($type, 'Artifact') !== false) {
            $color = 'Colorless';
        } elseif (strpos($colors, ',') !== false) {
            $color = 'Multi';
        } elseif ($colors === 'W' || $colorIdentity === 'W') {
            $color = 'White';
        } elseif ($colors === 'B' || $colorIdentity === 'B') {
            $color = 'Blue';
        } elseif ($colors === 'U' || $colorIdentity === 'U') {
            $color = 'Black';
        } elseif ($colors === 'G' || $colorIdentity === 'G') {
            $color = 'Green';
        } elseif ($colors === 'R' || $colorIdentity === 'R') {
            $color = 'Red';
        } else {
//            if (in_array($card['number'], ['1', '546'])) {
                $color = 'Colorless';
                error_log('colorが設定できない: ' . $name . PHP_EOL, 3, '/var/www/html/caketest/logs/error.log');
//            } else {
//                var_dump($card);
//                var_dump($colors);
//                var_dump($colorIdentity);
//                die('colorが設定できない');
//            }
        }

        $stmt->bind_param("ssssssssssssssssssssss", $name, $name_jp, $rarity, $color, $colorIdentity, $colors, $flavorText, $flavorText_jp, $text, $text_jp, $type, $type_jp, $types, $layout, $subtypes, $supertypes, $code, $borderColor, $frameVersion, $keywords, $faceName, $side);
        $stmt->execute();
    }

    foreach ($cardset['tokens'] as $card) {
        $name = $card['name'];
        $name_jp = (isset($card['foreignData'][4]['name'])) ? $card['foreignData'][4]['name'] : null ;
        $name_kana = null;
        $rarity = (isset($card['rarity'])) ? $card['rarity'] : null ;
        $color = null;
        $colorIdentity = (count($card['colorIdentity'])) ? implode(',', get_object_vars($card['colorIdentity'])) : null ;
        $colors = (count($card['colors'])) ? implode(',', get_object_vars($card['colors'])) : null ;
        $flavorText = (isset($card['flavorText'])) ? $card['flavorText'] : null ;
        $flavorText_jp = (isset($card['foreignData'][4]['flavorText'])) ? $card['foreignData'][4]['flavorText'] : null ;
        $text = (isset($card['flavorText'])) ? $card['flavorText'] : null ;
        $text_jp = (isset($card['foreignData'][4]['text'])) ? $card['foreignData'][4]['text'] : null ;
        $type = $card['type'];
        $type_jp = (isset($card['foreignData'][4]['type'])) ? $card['foreignData'][4]['type'] : null ;
        $types = (count($card['types'])) ? implode(',', get_object_vars($card['types'])) : null ;
        $layout = (isset($card['layout'])) ? $card['layout'] : null ;
        $subtypes = (count($card['subtypes'])) ? implode(',', get_object_vars($card['subtypes'])) : null ;
        $supertypes = (count($card['supertypes'])) ? implode(',', get_object_vars($card['supertypes'])) : null ;
        $code = (isset($card['code'])) ? $card['code'] : null ;
        $borderColor = (isset($card['borderColor'])) ? $card['borderColor'] : null ;
        $frameVersion = (isset($card['frameVersion'])) ? $card['frameVersion'] : null ;
        $keywords = (count($card['supertypes'])) ? implode(',', get_object_vars($card['supertypes'])) : null ;
        $faceName = (isset($card['faceName'])) ? $card['faceName'] : null ;
        $side = (isset($card['side'])) ? $card['side'] : null ;

        if (!$colors && strpos($type, 'Land') !== false) {
            $color = 'Colorless';
        } elseif (!$colors && strpos($type, 'Artifact') !== false) {
            $color = 'Colorless';
        } elseif (strpos($colors, ',') !== false) {
            $color = 'Multi';
        } elseif ($colors === 'W' || $colorIdentity === 'W') {
            $color = 'White';
        } elseif ($colors === 'B' || $colorIdentity === 'B') {
            $color = 'Blue';
        } elseif ($colors === 'U' || $colorIdentity === 'U') {
            $color = 'Black';
        } elseif ($colors === 'G' || $colorIdentity === 'G') {
            $color = 'Green';
        } elseif ($colors === 'R' || $colorIdentity === 'R') {
            $color = 'Red';
        } else {
//            if (in_array($card['number'], ['1', '546'])) {
            $color = 'Colorless';
            error_log('colorが設定できない: ' . $name . PHP_EOL, 3, '/var/www/html/caketest/logs/error.log');
//            } else {
//                var_dump($card);
//                var_dump($colors);
//                var_dump($colorIdentity);
//                die('colorが設定できない');
//            }
        }

        $stmt->bind_param("ssssssssssssssssssssss", $name, $name_jp, $rarity, $color, $colorIdentity, $colors, $flavorText, $flavorText_jp, $text, $text_jp, $type, $type_jp, $types, $layout, $subtypes, $supertypes, $code, $borderColor, $frameVersion, $keywords, $faceName, $side);
        $stmt->execute();
    }

}

$stmt->close();

$mysqli->close();

