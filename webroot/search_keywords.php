<?php
ini_set('display_errors', 1);
error_reporting(-1);

$search_keywords = ['ZNR', 'Sea', 'Sea Gate', 'Sea Gate Banneret'];

$searchwords = ['ZNR', 'Sea', 'Gate', 'Banneret'];

function hasKeywords ($keyword)
{
    global $search_keywords;
    return in_array($keyword, $search_keywords);
}

$analyzed = [];
$sequence = [];
$cnt = 0;
function recursive ($word)
{
    global $searchwords;
    global $analyzed;
    global $sequence;
    global $cnt;
    $sequence[] = $searchwords[$cnt];
    $sequence_imploded = implode(' ', $sequence);
    if (count($searchwords) - 1 >= $cnt) {
        if (hasKeywords($sequence_imploded)) {
            $cnt+= 1;
        } else {
            array_pop($sequence);
            $analyzed[] = implode(' ', $sequence);
            $sequence = [];
        }
        if (isset($searchwords[$cnt])) {
            recursive($searchwords[$cnt]);
        } else {
            $analyzed[] = implode(' ', $sequence);
        }
    }
}

recursive($searchwords[$cnt]);

var_dump($analyzed);
