<?php
use Migrations\AbstractMigration;

class CreateSearchMultiFulltext extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('search_multi_fulltext');
        $table->addColumn('name', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('name_jp', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('card_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->create();
    }
}
